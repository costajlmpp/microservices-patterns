# Build
mvn clean package && docker build -t io.costax/monitoring .

# RUN

docker rm -f monitoring || true && docker run -d -p 8080:8080 -p 4848:4848 --name monitoring io.costax/monitoring 