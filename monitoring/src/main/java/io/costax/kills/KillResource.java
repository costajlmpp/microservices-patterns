package io.costax.kills;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("kill")
@RequestScoped
public class KillResource {

    @POST
    public void doKill() {
        System.exit(-1);
    }

}
