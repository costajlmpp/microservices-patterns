# Requirements


This demo is using docker images and containers, so a fully working docker environment is expected. The docker command needs to be executable, so you can try this command to see if everything is ok.

```
docker version
```

# Steps

## 1. create a docker network

```
docker network create demo-net
```

## 2. build the app project

```
mvn clean package && docker build -t io.costax/monitoring .
```

```
docker run -d -p 8080:8080 --name demoapp  io.costax/monitoring
```

or
```
docker run -d -p 8080:8080 --name demoapp --net demo-net io.costax/monitoring
```

The name **demoapp** is here important as it is used in the Prometheus configuration. We have defined that it looks up the application through DNS by using the names service.


You can verify if everything is ok by calling the following URL in your browser

```
http://localhost:8080/monitoring/resources/ping
```

and see the metrics

```
http://localhost:8080/metrics
```


## 3. Create Prometheus image

```
cd docker-env/prometheus

```

```
docker build -t io.costax/prometheus .
```

```
docker run -d -p 9090:9090 --name prometheus --net demo-net io.costax/prometheus:latest
```

1. Open the browser with the URL http://localhost:9090.
2. Select the metrics _vendor:system_cpu_load_ in the drop-down.
3. Put some load on your machine with the badly written (on purpose) multi-threaded check with prime numbers on `http://localhost:8080/monitoring/resources/primes`.
4. You should see the spike in CPU usage after you have pressed the _execute_ button.

 
## 4. Start the Grafana instance
 
 
For the Grafana instance, no specific image or configuration is required. We can just run the default image and define the Prometheus instance within the GUI.
Within the grafana terminal, you can run the following command.

```
docker run -d --name grafana --net demo-net -p 3000:3000 grafana/grafana:6.2.4 
```

When we connect Grafana to Prometheus, it is best that we do this directly through the `demo-net` we created within docker. So we can find out this address of the Prometheus instance by running the following command

```
docker inspect demo-net
```

And look for the IP address of the Prometheus instance.

###### Configure/use Grafana

When using Grafana, you can look up the usage on the internet as this is nothing specific for MicroProfile metrics.
Here a few quick getting started things

- Open the browser with the URL `http://localhost:3000`
- Login with the default `admin/admin username/password` combination.
- Change the password to something personal.
- Add a data source to the Prometheus server, using that IP address that you discovered in the previous step
- Add a dashboard and graph, using the metric `vendor:system_cpu_load` as an example, the same as we used above.
