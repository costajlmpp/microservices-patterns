package io.costax.bookstore.books.entity;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class BookForm {


    @NotBlank
    private String title;

    @NotNull
    private BigDecimal price;

    @NotNull
    private Integer authorId;


    private BookForm(@NotBlank final String title, @NotNull final BigDecimal price, @NotNull final Integer authorId) {
        this.title = title;
        this.price = price;
        this.authorId = authorId;
    }

    @JsonbCreator
    public static BookForm of(
            @NotBlank @JsonbProperty("title") String title,
            @NotNull @JsonbProperty("price") BigDecimal price,
            @NotNull @JsonbProperty("authorId") Integer authorId) {
        return new BookForm(title, price, authorId);
    }


    public String getTitle() {
        return title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Integer getAuthorId() {
        return authorId;
    }
}
