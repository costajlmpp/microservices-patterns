package io.costax.bookstore.books.control;

import io.costax.bookstore.books.entity.Author;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class Authors {

    @Inject
    EntityManager em;

    public List<Author> all() {

        return em.createQuery("select a from Author a order by a.id", Author.class).getResultList();
    }

    @Transactional
    public Author add(Author author) {
        em.persist(author);
        em.flush();
        return author;
    }

    public Author get(final Integer id) {
        return em.find(Author.class, id);
    }
}
