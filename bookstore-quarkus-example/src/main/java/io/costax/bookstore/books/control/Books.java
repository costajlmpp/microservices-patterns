package io.costax.bookstore.books.control;

import io.costax.bookstore.books.entity.Author;
import io.costax.bookstore.books.entity.Book;
import io.costax.bookstore.books.entity.BookForm;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class Books {

    @Inject
    EntityManager em;

    public List<Book> all() {
        return em.createQuery("select b from Book b left join fetch order by b.id", Book.class).getResultList();
    }

    public Book get(final Integer id) {
        return em.find(Book.class, id);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public Book add(Book book) {
        final Author author = getAuthorOrThrow(Optional.ofNullable(book.getAuthor()).map(Author::getId).orElse(null));
        final Book newOne = Book.of(book.getTitle(), book.getPrice(), author);

        return persistBook(newOne);
    }

    private Book persistBook(final Book newOne) {
        em.persist(newOne);

        return newOne;
    }

    private Author getAuthorOrThrow(final Integer authorId) {
        return Optional.ofNullable(authorId)
                .map(id -> em.getReference(Author.class, id))
                .orElseThrow(() -> new IllegalArgumentException("Author not found"));
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public Book add(final BookForm bookForm) {
        final Author author = getAuthorOrThrow(bookForm.getAuthorId());
        final Book newOne = Book.of(bookForm.getTitle(), bookForm.getPrice(), author);

        return persistBook(newOne);
    }
}
