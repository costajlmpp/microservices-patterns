package io.costax.bookstore.books.boundary;

import io.costax.bookstore.books.control.Authors;
import io.costax.bookstore.books.entity.Author;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RequestScoped
@Path("/authors")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class AuthorsResource {

    @Inject
    Authors authors;

    @Context
    UriInfo uriInfo;

    @GET
    public List<Author> all() {
        return authors.all();
    }

    @GET
    @Path("{id : \\d+}")
    public Author getById(@PathParam("id") Integer id) {
        return Optional.ofNullable(id)
                .map(authors::get)
                .orElseThrow(NotFoundException::new);
    }

    @POST
    public Response add(@Valid Author author) {
        final Author added = authors.add(author);

        final URI uri = uriInfo.getAbsolutePathBuilder().path("{id}").build(added.getId());
        return Response.created(uri).entity(added).build();
    }


}
