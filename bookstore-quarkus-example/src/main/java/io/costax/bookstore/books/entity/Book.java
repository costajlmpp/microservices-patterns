package io.costax.bookstore.books.entity;

import io.costax.bookstore.FieldAccessStrategy;

import javax.json.bind.annotation.JsonbVisibility;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@JsonbVisibility(FieldAccessStrategy.class)
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    private String title;

    @NotNull
    private BigDecimal price;

    @NotNull
    @ManyToOne
    private Author author;

    public Book() {
    }

    private Book(@NotBlank final String title, @NotNull final BigDecimal price, @NotNull final Author author) {
        this.title = title;
        this.price = price;
        this.author = author;
    }

    public static Book of(@NotBlank final String title, @NotNull final BigDecimal price, @NotNull final Author author) {
        return new Book(title, price, author);
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Author getAuthor() {
        return author;
    }


}
