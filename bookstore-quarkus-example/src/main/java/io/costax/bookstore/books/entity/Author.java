package io.costax.bookstore.books.entity;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
//@JsonbVisibility(FieldAccessStrategy.class)
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    private String name;

    @NotBlank
    @Email
    private String email;

//    @NotBlank
//    @JsonbTransient
//    private String pwd;

    public Author() {
    }

    private Author(@NotBlank final String name, @NotBlank @Email final String email) {
        this.name = name;
        this.email = email;
    }

    @JsonbCreator
    public static Author of(@NotBlank @JsonbProperty("name") String name, @NotBlank @Email @JsonbProperty("email") String email) {
        return new Author(name, email);
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
}
