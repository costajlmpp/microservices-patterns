package io.costax.bookstore.books.boundary;

import io.costax.bookstore.books.control.Books;
import io.costax.bookstore.books.entity.Book;
import io.costax.bookstore.books.entity.BookForm;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RequestScoped
@Path("/books")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BooksResource {

    @Inject
    Books books;

    @Context
    UriInfo uriInfo;

    @GET
    public List<Book> getAll() {
        return books.all();
    }

    @GET
    @Path("{id : \\d+}")
    public Book get(Integer id) {
        return Optional.ofNullable(id)
                .map(books::get)
                .orElseThrow(NotFoundException::new);
    }

    @POST
    public Response add(@Valid Book book) {
        final Book b = books.add(book);

        final URI uri = uriInfo.getAbsolutePathBuilder().path("{id}").build(b.getId());

        return Response.created(uri).entity(b).build();
    }

    @POST
    @Path("form")
    public Response add(@Valid BookForm bookForm) {


        final Book b = books.add(bookForm);

        //final URI uri = uriInfo.getBaseUriBuilder().path(BooksResource.class).path("{id}").build(b.getId());
        URI uri = uriInfo.getBaseUriBuilder()
                .path(BooksResource.class)
                .path(BooksResource.class, "get")
                .build(b.getId());
        //final URI uri = uriInfo.getAbsolutePathBuilder().path("{id}").build(b.getId());

        return Response.created(uri).entity(b).build();
    }
}
