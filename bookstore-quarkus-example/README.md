## 1. Running the application

```bash
mvnw compile quarkus:dev
```


## 2. see swagger documentation 

* [swagger-ui](http://localhost:8080/swagger-ui)
* [openapi](http://localhost:8080/openapi)

* curl -v http://localhost:8080/openapi --output bookstore-openapi


## 3. Packaging and run the application

The application is packaged using `./mvnw package`. It produces 2 jar files:

  - `bookstore-1.0-SNAPSHOT.jar` - containing just the classes and resources of the projects, it’s the regular artifact produced by the Maven build;

  - `bookstore-1.0-SNAPSHOT-runner.jar` - being an executable jar. Be aware that it’s not an über-jar as the dependencies are copied into the target/lib directory.

You can run the application using: `java -jar target/bookstore-1.0-SNAPSHOT-runner.jar`

    
   - The `Class-Path` entry of the `MANIFEST.MF` from the runner jar explicitly lists the jars from the lib directory. So if you want to deploy your application somewhere, you need to copy the runner jar as well as the lib directory.

   - Before running the application, don’t forget to stop the hot reload mode (hit `CTRL+C`), or you will have a port conflict.


## 4. [build a native](https://quarkus.io/guides/building-native-image-guide.html)



###### Requirements 

1. JDK 1.8+ installed with JAVA_HOME configured appropriately
2. GraalVM installed from the GraalVM web site. Using the community edition is enough. Version 19.0.2 is required.
3. Apache Maven 3.5.3+
4. The GRAALVM_HOME environment variable configured appropriately
5. The native-image tool must be installed; this can be done by running `gu install native-image` from your GraalVM directory
6. A working C developer environment (see the note below for details)  
7. A running Docker
8. The code of the application developed in the Getting Started Guide.
    
    

###### How to do 

Once you have downloaded GraalVM, expand the archive and set the GRAALVM_HOME variable to this location:

```
export GRAALVM_HOME=$HOME/Development/graalvm/
```

On MacOS, point the variable to the Home sub-directory:

```
export GRAALVM_HOME=$HOME/Development/graalvm/Contents/Home/
```


```bash
export JAVA_HOME=/Library/Java/JavaVirtualMachines/graalvm-ce-19.0.2/Contents/Home
export GRAALVM_HOME=/Library/Java/JavaVirtualMachines/graalvm-ce-19.0.2/Contents/Home

mvn package -Pnative`
```

In addition to the regular files, the build also produces target/getting-started-1.0-SNAPSHOT-runner. You can run it using: ./target/getting-started-1.0-SNAPSHOT-runner.

## Docker image

```

mvn package -Pnative -Dnative-image.docker-build=true
```






## 3. add extenxions


Basically we what to use all the Java EE dependencies out-of-the-box :



####### dependencies what give us the complete EE env and microporofile


io.quarkus:quarkus-resteasy
io.quarkus:quarkus-hibernate-orm
io.quarkus:quarkus-jsonb
io.quarkus:quarkus-jsonp
io.quarkus:quarkus-resteasy-jsonb
io.quarkus:quarkus-narayana-jta
io.quarkus:quarkus-elytron-security
io.quarkus:quarkus-scheduler
io.quarkus:quarkus-swagger-ui
io.quarkus:quarkus-hibernate-validator
io.quarkus:quarkus-undertow-websockets
io.quarkus:quarkus-smallrye-fault-tolerance
io.quarkus:quarkus-smallrye-metrics
io.quarkus:quarkus-smallrye-openapi
io.quarkus:quarkus-smallrye-jwt
io.quarkus:quarkus-smallrye-health
io.quarkus:quarkus-smallrye-opentracing



```
mvn quarkus:add-extension -Dextensions="io.quarkus:quarkus-resteasy,io.quarkus:quarkus-hibernate-orm,io.quarkus:quarkus-jsonb,io.quarkus:quarkus-jsonp,io.quarkus:quarkus-resteasy-jsonb,io.quarkus:quarkus-narayana-jta,io.quarkus:quarkus-elytron-security,io.quarkus:quarkus-scheduler,io.quarkus:quarkus-swagger-ui,io.quarkus:quarkus-hibernate-validator,io.quarkus:quarkus-undertow-websockets,io.quarkus:quarkus-smallrye-fault-tolerance,io.quarkus:quarkus-smallrye-metrics,io.quarkus:quarkus-smallrye-openapi,io.quarkus:quarkus-smallrye-jwt,io.quarkus:quarkus-smallrye-health,io.quarkus:quarkus-smallrye-opentracing"
```


####### Others 

io.quarkus:quarkus-jdbc-h2
io.quarkus:quarkus-jdbc-postgres



```
mvn quarkus:add-extension -Dextensions="io.quarkus:quarkus-smallrye-openapi, \
	io.quarkus:quarkus-jdbc-h2, \
	io.quarkus:quarkus-hibernate-validator, \
	io.quarkus:quarkus-hibernate-orm, \
	io.quarkus:quarkus-jsonb, \
	io.quarkus:quarkus-resteasy-jsonb"

```


