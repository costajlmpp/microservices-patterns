package io.costax.ping.boundary;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("ping")
@Produces(MediaType.APPLICATION_JSON)
public class PingResource {

    private static final Logger LOGGER = Logger.getLogger(PingResource.class.getSimpleName());

    @Inject
    @ConfigProperty(name = "message")
    String message;

    @Context
    UriInfo uriInfo;

    @Context
    HttpHeaders headers;

    @GET
    public JsonArray get() {
        LOGGER.log(Level.INFO, "handler GET request " + headers.getRequestHeaders());

        sleep();

        return Json.createArrayBuilder()
                .add(Json.createObjectBuilder().add("id", 1).add("name", "Foo-1"))
                .add(Json.createObjectBuilder().add("id", 2).add("name", "Foo-2"))
                .build();
    }

    private void sleep()  {
        try {
            Thread.sleep(4_000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @POST
    public Response create(JsonObject payload) {
        LOGGER.log(Level.INFO, "handler POST request: " + payload);

        sleep();

        URI uri = uriInfo.getAbsolutePathBuilder().path("{id}").build(123);

        JsonObject responsePayload = Json.createObjectBuilder().add("created", LocalDateTime.now().toString())
                .add("x", payload)
                .build();

        return Response.created(uri).entity(responsePayload).build();
    }

    @POST
    @Path("bad-request")
    public Response badRequest(JsonObject payload) {

        return Response.status(Response.Status.BAD_REQUEST)
                .entity(Json.createObjectBuilder()
                        .add("reason", "this constrain reason")
                        .add("path", "payload.identifier")
                        .build()
                ).build();
    }


    @POST
    @Path("server-error")
    public Response serverError(JsonObject payload) {

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(Json.createObjectBuilder()
                        .add("reason", "some unexpected error, contact the administrators")
                        .add("path", "payload.identifier")
                        .build()
                ).build();
    }

    @DELETE
    public Response delete() {
        return Response.noContent().build();
    }



}
