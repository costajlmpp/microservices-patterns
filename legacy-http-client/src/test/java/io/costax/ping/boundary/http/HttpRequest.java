package io.costax.ping.boundary.http;

import java.net.URL;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

/**
 * @author costax
 */
public class HttpRequest {

    private final URL uri;
    private final byte[] payload;
    private Duration timeout = Duration.ofMinutes(1L);
    private Map<String, String> headers = new HashMap<>();
    private Method method = Method.GET;

    private HttpRequest(final Builder builder) {
        this.uri = builder.url;
        this.timeout = builder.timeout;
        this.headers = builder.headers;
        this.method = builder.method;
        this.payload = builder.payload;
    }

    public static Builder build() {
        return new Builder();
    }

    public Method getMethod() {
        return method;
    }

    public URL getUri() {
        return uri;
    }

    public Duration getTimeout() {
        return timeout;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public byte[] getPayload() {
        return payload;
    }

    public enum Method {
        GET, POST, PUT, DELETE, PATCH, HEAD, OPTIONS
    }

    public static class Builder {
        private final Map<String, String> headers = new HashMap<>();
        private URL url;
        private Duration timeout = Duration.ZERO;
        private HttpRequest.Method method = Method.GET;
        private byte[] payload = null;

        Builder() {
            accept("*/*");
        }

        public Builder url(final URL url) {
            this.url = url;
            return this;
        }

        public Builder accept(String acceptType) {
            header("Accept", acceptType);
            return this;
        }

        public Builder contentType(String contentType) {
            return header("Content-Type", contentType);
        }

        public Builder timeout(final Duration timeout) {
            this.timeout = timeout;
            return this;
        }

        public Builder headers(final Map<String, String> headers) {
            this.headers.putAll(headers);
            return this;
        }

        public Builder header(String key, String value) {
            this.headers.put(key, value);
            return this;
        }

        public Builder payload(final byte[] payload) {
            this.payload = payload;
            return this;
        }

        public HttpRequest createHttpRequest() {
            return new HttpRequest(this);
        }

        public HttpRequest get() {
            this.method = HttpRequest.Method.GET;
            return new HttpRequest(this);
        }

        public HttpRequest post(byte[] payload) {
            this.method = Method.POST;
            this.payload = payload;
            return new HttpRequest(this);
        }

        public HttpRequest delete() {
            this.method = Method.DELETE;
            //this.payload = payload;
            return new HttpRequest(this);
        }

        public HttpRequest put(byte[] payload) {
            this.method = Method.PUT;
            this.payload = payload;
            return new HttpRequest(this);
        }

        public HttpRequest head() {
            this.method = Method.HEAD;
            return new HttpRequest(this);
        }

        public HttpRequest options() {
            this.method = Method.OPTIONS;
            return new HttpRequest(this);
        }
    }
}
