package io.costax.ping.boundary.http;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.net.HttpURLConnection;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * HttpClient build = HttpClient.newBuilder()
 * //.authenticator() TODO:
 * .connectTimeout(Duration.ofMillis(500L))
 * .executor(Executors.newFixedThreadPool(2))
 * .build();
 *
 * @author costax
 */
public class HttpClient {

    private final Duration connectTimeout;
    private final ExecutorService executor;

    public HttpClient(final Duration connectTimeout, final ExecutorService executorService) {
        this.connectTimeout = connectTimeout;
        this.executor = executorService;
    }

    public static HttpClient newHttpClient() {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        return new HttpClient(Duration.ofMinutes(1L), executorService);
    }

    public HttpResponse send(HttpRequest request) {
        try {

            return sendAsync(request).get();

        } catch (InterruptedException e) {

            // TODO: 14/05/2020 use logger

            e.printStackTrace();

            throw new RuntimeException(e);

        } catch (ExecutionException e) {

            // TODO: 14/05/2020 use logger
            e.printStackTrace();

            if (e.getCause() != null) {
                if (e.getCause() instanceof RuntimeException) {
                    throw (RuntimeException) e.getCause();
                }
            }

            // TODO: 2020-05-14 - we can create other exception
            throw new RuntimeException(e);
        }
    }

    public CompletableFuture<HttpResponse> sendAsync(HttpRequest request) {
        Objects.requireNonNull(request);
        Objects.requireNonNull(request.getMethod());
        return CompletableFuture.supplyAsync(() -> execute(request), this.executor);
    }

    /*
     * @throws java.net.SocketTimeoutException when the readTimeOut was extends
     */
    private HttpResponse execute(final HttpRequest request) {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) request.getUri().openConnection();
            connection.setRequestMethod(request.getMethod().toString());
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setConnectTimeout((int) connectTimeout.toMillis());
            connection.setReadTimeout((int) request.getTimeout().toMillis());
            addHeaders(connection, request);

            connection.connect();
            // write the request payload
            addRequestBody(request, connection);

            return getResponse(connection);

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    private HttpResponse getResponse(final HttpURLConnection connection) throws IOException {
        int responseCode = connection.getResponseCode();
        final String responseMessage = connection.getResponseMessage();

        String statusLine = connection.getHeaderField(null); // status Line;
        String protocolVersion = statusLine.split(" ")[0];
        final Map<String, List<String>> headerFields = connection.getHeaderFields();

        final HttpResponse.StatusFamily statusFamily = HttpResponse.StatusFamily.of(connection.getResponseCode());
        byte[] body = getResponseBody(statusFamily, connection);

        return new HttpResponse(protocolVersion, responseCode, responseMessage, headerFields, body);
    }

    private byte[] getResponseBody(final HttpResponse.StatusFamily statusFamily, final HttpURLConnection connection) throws IOException {
        try (InputStream inputStream = getResponseInputBody(statusFamily, connection)) {
            return inputStream.readAllBytes();
        }
    }

    private InputStream getResponseInputBody(final HttpResponse.StatusFamily statusFamily, HttpURLConnection connection) throws IOException {
        return switch (statusFamily) {
            case INFORMATIONAL, SUCCESSFUL, REDIRECTION -> connection.getInputStream();
            case CLIENT_ERROR, SERVER_ERROR -> connection.getErrorStream();
            case OTHER -> throw new IllegalStateException();
        };
    }

    private void addRequestBody(final HttpRequest request, final HttpURLConnection connection) throws IOException {
        if (request.getPayload() != null) {
            try (OutputStream os = connection.getOutputStream()) {
                os.write(request.getPayload(), 0, request.getPayload().length);
            }
        }
    }

    private void addHeaders(HttpURLConnection connection, HttpRequest request) {
        request.getHeaders().forEach(connection::setRequestProperty);
    }
}
