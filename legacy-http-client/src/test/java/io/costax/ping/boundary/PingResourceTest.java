package io.costax.ping.boundary;

import io.costax.ping.boundary.http.HttpClient;
import io.costax.ping.boundary.http.HttpRequest;
import io.costax.ping.boundary.http.HttpResponse;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.json.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;

/**
 * @author costax
 */
public class PingResourceTest {

    public static final String HTTP_LOCALHOST_8080_BOOKS_API_RESOURCES_PING = "http://localhost:8080/books-api/resources/ping";
    static HttpClient HTTP_CLIENT;
    static URL URL;

    @BeforeClass
    public static void beforeClass() throws Exception {
        HTTP_CLIENT = HttpClient.newHttpClient();
        URL = new URL(HTTP_LOCALHOST_8080_BOOKS_API_RESOURCES_PING);
    }

    @Test
    public void acceptWildCard() throws MalformedURLException {
        HttpRequest request = HttpRequest.build()
                .url(new URL("http://localhost:9990/metrics"))
                .contentType("*/*")
                .accept("*/*")
                .get();

        HttpResponse response = HTTP_CLIENT.send(request);

        printResponse(response);
    }

    @Test
    public void getReadEntity() {
        final HttpRequest request = HttpRequest
                .build()
                .timeout(Duration.ofMillis(6000L))
                .header("X-FUNNY", "just-dummy")
                .url(URL)
                .get();

        HttpResponse response = HTTP_CLIENT.send(request);

        JsonArray jsonValues = response.readEntity(this::read).orElse(null);

        printResponse(response);
    }

    byte[] serialize(JsonObject object) {
        try (ByteArrayOutputStream oos = new ByteArrayOutputStream(); JsonWriter writer = Json.createWriter(oos)) {
            writer.writeObject(object);
            writer.close();
            oos.flush();
            return oos.toByteArray();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public JsonArray read(byte[] bytes) {
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
             JsonReader reader = Json.createReader(byteArrayInputStream)) {
            return reader.readArray();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Test
    public void doGet() {
        final HttpRequest request = HttpRequest
                .build()
                .timeout(Duration.ofMillis(6000L))
                .header("X-FUNNY", "just-dummy")
                .url(URL)
                .get();

        HttpResponse response = HTTP_CLIENT.send(request);

        printResponse(response);
    }

    @Test
    public void doPost() {
        String jsonInputString = "{\"name\": \"Duke\", \"jobs\": [\"Developer\", \"Thinker\"]}";

        HttpRequest request = HttpRequest
                .build()
                .timeout(Duration.ofMillis(5000L))
                .header("X-FUNNY", "just-dummy")
                .url(URL)
                .contentType("application/json")
                .post(jsonInputString.getBytes(StandardCharsets.UTF_8));

        HttpResponse response = HTTP_CLIENT.send(request);

        printResponse(response);
    }

    @Test
    public void doPostAsync() {

        String jsonInputString = "{\"name\": \"Duke\", \"jobs\": [\"Developer\", \"Thinker\"]}";

        HttpRequest request = HttpRequest
                .build()
                .timeout(Duration.ofMillis(5000L))
                .header("X-FUNNY", "just-dummy")
                .url(URL)
                .contentType("application/json")
                .post(jsonInputString.getBytes(StandardCharsets.UTF_8));

        CompletableFuture<HttpResponse> httpResponseCompletableFuture = HTTP_CLIENT.sendAsync(request);

        httpResponseCompletableFuture.join();
    }

    @Test
    public void handlerBadRequest() throws IOException {
        String jsonInputString = "{\"name\": \"Duke\", \"jobs\": [\"Developer\", \"Thinker\"]}";

        HttpRequest request = HttpRequest
                .build()
                .timeout(Duration.ofMillis(5000L))
                .header("X-FUNNY", "just-dummy")
                .url(new URL(HTTP_LOCALHOST_8080_BOOKS_API_RESOURCES_PING + "/bad-request"))
                .contentType("application/json")
                .post(jsonInputString.getBytes(StandardCharsets.UTF_8));

        HttpResponse response = HTTP_CLIENT.send(request);

        printResponse(response);
    }

    @Test
    public void serverError() throws IOException {
        String jsonInputString = "{\"name\": \"Duke\", \"jobs\": [\"Developer\", \"Thinker\"]}";

        HttpRequest request = HttpRequest
                .build()
                .timeout(Duration.ofMillis(5000L))
                .header("X-FUNNY", "just-dummy")
                .url(new URL(HTTP_LOCALHOST_8080_BOOKS_API_RESOURCES_PING + "/server-error"))
                .contentType("application/json")
                .post(jsonInputString.getBytes(StandardCharsets.UTF_8));

        HttpResponse response = HTTP_CLIENT.send(request);

        printResponse(response);
    }

    @Test
    public void notContentNoBody() {
        HttpRequest request = HttpRequest
                .build()
                .timeout(Duration.ofMillis(5000L))
                .header("X-FUNNY", "just-dummy")
                .url(URL)
                .contentType("application/json")
                .delete();

        HttpResponse response = HTTP_CLIENT.send(request);

        printResponse(response);
    }

    @Test
    public void head() {
        HttpRequest request = HttpRequest
                .build()
                .timeout(Duration.ofMillis(5000L))
                .header("X-FUNNY", "just-dummy")
                .url(URL)
                .contentType("application/json")
                .head();

        HttpResponse response = HTTP_CLIENT.send(request);

        printResponse(response);
    }

    @Test
    public void options() {
        HttpRequest request = HttpRequest
                .build()
                .timeout(Duration.ofMillis(5000L))
                .header("X-FUNNY", "just-dummy")
                .url(URL)
                .contentType("application/json")
                .options();

        HttpResponse response = HTTP_CLIENT.send(request);

        printResponse(response);
    }

    private void printResponse(final HttpResponse httpResponse) {
        System.out.println("> " + httpResponse.getStatusLine());
        httpResponse.getHeaders()
                .forEach((key, value) -> System.out.println("> " + key + ": " + value));
        System.out.println("> ");

        String entity = httpResponse
                .readEntity(bytes -> new String(httpResponse.getBody(), StandardCharsets.UTF_8)).orElse(null);

        System.out.println("> " + entity);
    }


}
