package io.costax.ping.boundary;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.json.JsonArray;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.concurrent.TimeUnit;

public class PingResourceWithJaxRsClientTest {

    public static final String HTTP_LOCALHOST_8080_BOOKS_API_RESOURCES_PING = "http://localhost:8080/books-api/resources/ping";

    private Client client;
    private WebTarget target;

    @Before
    public void setUp() {
        client = ClientBuilder.newBuilder()
                .connectTimeout(2_000, TimeUnit.MILLISECONDS)
                //.property("connection.timeout", 100)
                //.sslContext(sslContext)
                //.register(JacksonJsonProvider.class)
                .build();
        target = client.target(HTTP_LOCALHOST_8080_BOOKS_API_RESOURCES_PING);
    }

    @After
    public void tearDown() {
        client.close();
    }

    @Test
    public void name() {
        JsonArray jsonObject;
        try (Response response = target.request().get()) {

            System.out.println(response);
            Response.Status.Family family = response.getStatusInfo().getFamily();

            jsonObject = response.readEntity(JsonArray.class);
        }

        System.out.println(jsonObject);
    }
}
