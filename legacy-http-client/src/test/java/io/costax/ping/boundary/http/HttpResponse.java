package io.costax.ping.boundary.http;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toUnmodifiableMap;

/**
 * @author costax
 */
public final class HttpResponse {

    private final Headers headers;
    private final byte[] body;

    protected HttpResponse(final String protocolVersion,
                           final int statusCode,
                           final String responseMessage,
                           final Map<String, List<String>> headers,
                           final byte[] body) {
        this.headers = new Headers(protocolVersion, statusCode, responseMessage, headers);
        this.body = body;
    }

    public String getStatusLine() {
        return headers.getStatusLine();
    }

    public int statusCode() {
        return headers.getStatusCode();
    }

    public Map<String, List<String>> getHeaders() {
        return headers.getHeaders();
    }

    public byte[] getBody() {
        if (body == null) return null;
        return Arrays.copyOf(this.body, this.body.length);
    }

    public <T> Optional<T> readEntity(Function<byte[], T> bodyMapper) {
        return Optional.ofNullable(getBody())
                .map(bodyMapper);
    }

    public StatusFamily getStatusFamily() {
        return StatusFamily.of(this.headers.getStatusCode());
    }

    @Override
    public String toString() {
        return "HttpResponse{" + headers + '}';
    }

    public enum StatusFamily {
        INFORMATIONAL(1), SUCCESSFUL(2), REDIRECTION(3), CLIENT_ERROR(4), SERVER_ERROR(5), OTHER(0);

        private final int family;

        StatusFamily(final int family) {
            this.family = family;
        }

        static StatusFamily of(int statusCode) {
            final int familyIdentifier = statusCode / 100;
            return Arrays.stream(values())
                    .filter(statusFamily -> statusFamily.family == familyIdentifier)
                    .findFirst()
                    .orElse(OTHER);
        }

    }

    private static class Headers {
        private final String protocolVersion;
        private final int statusCode;
        private final String responseMessage;
        private final Map<String, List<String>> headers;

        Headers(final String protocolVersion,
                final int statusCode,
                final String responseMessage,
                final Map<String, List<String>> headers) {
            this.protocolVersion = protocolVersion;
            this.statusCode = statusCode;
            this.responseMessage = responseMessage;
            this.headers = headers.entrySet()
                    .stream()
                    .filter(e -> nonNull(e.getKey()))
                    .collect(toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue));
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + " { " + getStatusLine() + " }";
        }

        public String getProtocolVersion() {
            return protocolVersion;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public String getResponseMessage() {
            return responseMessage;
        }

        public Map<String, List<String>> getHeaders() {
            return headers;
        }

        /**
         * The first line of a Response message is the Status-Line,
         * consisting of the protocol version followed by a numeric status code and its associated textual phrase,
         * with each element separated by SP characters.
         * No CR or LF is allowed except in the final CRLF sequence.
         *
         * @return protocolVersion statusCode statusMessage
         */
        public String getStatusLine() {
            return protocolVersion + " " + statusCode + " " + responseMessage;
        }
    }
}
