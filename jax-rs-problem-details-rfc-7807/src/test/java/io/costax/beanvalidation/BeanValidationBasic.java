package io.costax.beanvalidation;

import org.junit.jupiter.api.Test;

import javax.validation.Configuration;
import javax.validation.MessageInterpolator;
import javax.validation.ParameterNameProvider;
import javax.validation.Validation;

public class BeanValidationBasic {


    @Test
    void name() {

        Configuration<?> configure = Validation.byDefaultProvider()
                .configure();

        MessageInterpolator defaultMessageInterpolator = configure.getDefaultMessageInterpolator();

        System.out.println(defaultMessageInterpolator.getClass());

        ParameterNameProvider defaultParameterNameProvider = configure.getDefaultParameterNameProvider();


        System.out.println(defaultParameterNameProvider.getClass());

        // new ResourceBundleMessageInterpolator()


//        Validator validator = configure
//                .messageInterpolator(
//                        new ResourceBundleMessageInterpolator(
//                                new PlatformResourceBundleLocator( "MyMessages" )
//                        )
//                )
//                .buildValidatorFactory()
//                .getValidator();
    }
}
