package io.costax;

import io.costax.core.messages.ResourceBundleMessageSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleLoadingTest {

    private static final Locale PT_PT = new Locale("pt", "PT");
    private static final Locale PT = new Locale("pt");
    private static final Locale EN = new Locale("en");
    private static final Locale ES = new Locale("es");

    static ResourceBundleMessageSource messages;


    @BeforeAll
    static void beforeAll() {
        messages = ResourceBundleMessageSource.of("messages");
    }

    @Test
    void load() {


        ResourceBundle messagesPT = ResourceBundle.getBundle("messages", PT_PT);
        ResourceBundle messagesEN = ResourceBundle.getBundle("messages", EN);
        ResourceBundle messagesES = ResourceBundle.getBundle("messages", ES);

        System.out.println("-->" + messagesPT.getLocale());
        System.out.println("-->" + messagesEN.getLocale());
        System.out.println("-->" + messagesES.getLocale());

        System.out.println(messagesPT.getString("book.isbn"));
        System.out.println(messagesEN.getString("book.isbn"));
    }

    @Test
    void name() {

       // ResourceBundle.Control rbc = ResourceBundle.Control.getControl(ResourceBundle.Control.FORMAT_DEFAULT);

        //List<Locale> messages = rbc.getCandidateLocales("messages", PT);


        ResourceBundle messagesPT = ResourceBundle.getBundle("messages", PT, new ModifiedControl());

        Assertions.assertEquals("Livro isbn", messagesPT.getString("book.isbn"));
    }

    public class ModifiedControl extends ResourceBundle.Control {

        @Override
        public List<Locale> getCandidateLocales(final String baseName, final Locale locale) {
            List<Locale> candidateLocales = super.getCandidateLocales(baseName, locale);

            Locale pt = new Locale("pt");
            if (pt.equals(locale)) {
                Locale ptPT = new Locale("pt", "PT");
                if (!candidateLocales.contains(ptPT)) {
                    candidateLocales.add(ptPT);
                }
            }

            return candidateLocales;
        }

    }

    @Test
    void resolveCodeWithoutArguments() {



        Assertions.assertEquals("Livro isbn", messages.getMessage("book.isbn", PT_PT));
        Assertions.assertEquals("Livro isbn", messages.getMessage("book.isbn", PT));
        Assertions.assertEquals("Book Isbn", messages.getMessage("book.isbn", EN));
        Assertions.assertEquals("book.missing-key", messages.getMessage("book.missing-key", PT_PT));
    }

    @Test
    void resolveCodeWithArguments() {
        // person.message.text={0} is a simple person who quotes {1} animals one of which is a Cat named {2}!!!


        System.out.println(messages.getMessage("person.message.text", new Object[]{"Pedro", 5, "Teco"}, EN));
        System.out.println(messages.getMessage("person.message.text", new Object[]{"Pedro", 5, "Teco"}, PT_PT));
    }
}
