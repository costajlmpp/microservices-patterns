package io.costax.books.boundary;

import io.costax.books.control.AuthorRegistrationService;
import io.costax.books.control.AuthorRepository;
import io.costax.books.entity.Author;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

@Path("/authors")
@Produces(MediaType.APPLICATION_JSON)
public class AuthorsResource {

    @Context
    UriInfo uriInfo;

    @Inject
    AuthorRegistrationService service;

    @Inject
    AuthorRepository repository;

    @POST
    public Response create(@Valid Author payload) {
        Author author = service.create(payload);
        Long id = author.getId();
        URI uri = uriInfo.getAbsolutePathBuilder().path(AuthorsResource.class, "getById").build(id);
        return Response.created(uri).entity(author).build();
    }

    @GET
    @Path("/{id :\\d+}")
    public Author getById(@PathParam("id") Long id) {
        return repository.findByIdElseThrow(id);
    }

}
