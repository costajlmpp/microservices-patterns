package io.costax.books.boundary;

import io.costax.books.control.BookRegistationService;
import io.costax.books.control.BookRepository;
import io.costax.books.entity.Book;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

@Path("/books")
@Produces(MediaType.APPLICATION_JSON)
public class BooksResource {

    @Inject
    BookRepository repository;

    @Inject
    BookRegistationService registationService;

    @Context
    UriInfo uriInfo;

    @GET
    public List<Book> list() {
        return repository.findAll();
    }

    @GET
    @Path("/{id:\\d+}")
    public Book getById(@PathParam("id") Long id) {
        return repository.findById(id);
    }

    @POST
    public Response add(@Valid Book payload) {
        Book newBook = registationService.add(payload);

        URI uri = uriInfo.getAbsolutePathBuilder().path("{id}").build(newBook.getId());

        return Response.created(uri).entity(newBook).build();
    }

    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createPerson(
            @FormParam("id")
            @NotNull
            @Pattern(regexp = "[0-9]+")
                    String id,
            @FormParam("name")
            @Size(min = 2, max = 50)
                    String name) {

        JsonObject jsonObject = Json
                .createObjectBuilder()
                .add("id", Integer.parseInt(id))
                .add("name", name)
                .build();


        return Response.status(Response.Status.CREATED).entity(jsonObject).build();
    }
}
