package io.costax.books.control;

import io.costax.books.entity.Book;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;


public class BookRepository {

    @Inject
    EntityManager em;

    public List<Book> findAll() {
        return em.createQuery("select b from Book b", Book.class).getResultList();
    }

    public Book findById(final Long id) {
        return em.find(Book.class, id);
    }
}
