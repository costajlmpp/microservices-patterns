package io.costax.books.control;

import io.costax.books.entity.Author;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class AuthorRegistrationService {

    @Inject AuthorRepository repository;

    public Author create(Author request) {
        return repository.add(request);
    }
}
