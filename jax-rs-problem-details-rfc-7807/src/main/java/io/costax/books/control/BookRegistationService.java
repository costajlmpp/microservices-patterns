package io.costax.books.control;

import io.costax.books.entity.Book;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@Stateless
public class BookRegistationService {

    @Inject
    EntityManager em;

    //@Transactional
    public Book add(Book request) {
        Book merge = em.merge(request);
        em.flush();

        return merge;
    }


}
