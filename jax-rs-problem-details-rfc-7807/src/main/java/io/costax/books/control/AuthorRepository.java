package io.costax.books.control;


import io.costax.books.entity.Author;
import io.costax.core.persistence.NoResourceException;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.Optional;

public class AuthorRepository {

    @Inject
    EntityManager em;


    public Author add(Author author) {
        em.persist(author);
        em.flush();
        return author;
    }


    public Optional<Author> findById(final Long id) {
        return Optional.ofNullable(em.find(Author.class, id));
    }

    public Author findByIdElseThrow(final Long id) {
        return findById(id).orElseThrow(() -> NoResourceException.of(Author.class, id));
    }
}
