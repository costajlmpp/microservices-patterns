package io.costax.core.validation;

import io.costax.core.locale.LocaleThreadLocal;

import javax.validation.MessageInterpolator;
import javax.validation.Validation;
import java.util.Locale;

/**
 * Delegates to a MessageInterpolator implementation but enforces a given Locale.
 */
public class LocaleSpecificMessageInterpolator implements javax.validation.MessageInterpolator {

    private final MessageInterpolator defaultInterpolator;

    public LocaleSpecificMessageInterpolator() {
        defaultInterpolator = Validation.byDefaultProvider().configure().getDefaultMessageInterpolator();
    }

    @Override
    public String interpolate(final String messageTemplate, final Context context) {
        return defaultInterpolator.interpolate(messageTemplate, context, LocaleThreadLocal.get());
    }

    @Override
    public String interpolate(final String messageTemplate, final Context context, final Locale locale) {
        return defaultInterpolator.interpolate(messageTemplate, context, LocaleThreadLocal.get());
    }
}
