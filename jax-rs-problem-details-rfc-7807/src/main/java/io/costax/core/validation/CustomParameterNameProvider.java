package io.costax.core.validation;

import javax.validation.ParameterNameProvider;
import javax.validation.Validation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * If method input parameters are invalid, this class returns actual parameter names instead of the default ones (
 * {@code arg0, arg1, ...})
 */
public class CustomParameterNameProvider implements ParameterNameProvider {

    private final ParameterNameProvider defaultParameterNameProvider;

    public CustomParameterNameProvider() {
        defaultParameterNameProvider = Validation.byDefaultProvider().configure().getDefaultParameterNameProvider();
    }

    @Override
    public List<String> getParameterNames(final Constructor<?> constructor) {
        return defaultParameterNameProvider.getParameterNames(constructor);
    }

    @Override
    public List<String> getParameterNames(final Method method) {
        // method.getDeclaringClass().getSimpleName()
        // Optionmethod.getDeclaringClass().getSimpleName().map(Introspector::decapitalize)

        String className = Optional.ofNullable(method.getDeclaringClass())
                .map(Class::getSimpleName)
                .orElse("");

        String methodName = method.getName();

        String methodNameSignature = Stream.of(className, methodName).filter(e -> !e.isBlank())
                .collect(Collectors.joining("."));
        int methodNumberOfParams = method.getParameters().length;

        if ("BooksResource.createPerson".equalsIgnoreCase(methodNameSignature) && 2 == methodNumberOfParams) {
            return List.of("id", "name");
        }

        if ("AuthorsResource.create".equalsIgnoreCase(methodNameSignature) && 1 == methodNumberOfParams) {
            return List.of("author");
        }

        return defaultParameterNameProvider.getParameterNames(method);
    }
}
