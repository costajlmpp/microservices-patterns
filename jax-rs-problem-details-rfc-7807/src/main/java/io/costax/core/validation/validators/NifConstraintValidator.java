package io.costax.core.validation.validators;

import io.costax.core.validation.constraints.Nif;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NifConstraintValidator implements ConstraintValidator<Nif, String> {

   private int size;

   @Override
   public void initialize(Nif constraint) {
      this.size = constraint.size();
   }

   @Override
   public boolean isValid(String obj, ConstraintValidatorContext context) {
      return obj != null && obj.length() == size;
   }
}
