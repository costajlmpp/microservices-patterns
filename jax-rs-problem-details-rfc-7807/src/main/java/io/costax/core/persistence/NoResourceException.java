package io.costax.core.persistence;

import javax.ejb.ApplicationException;
import java.io.Serializable;
import java.util.Objects;

@ApplicationException(rollback = true, inherited = true)
public class NoResourceException extends RuntimeException {

    private final Class<?> resourceType;
    private final Serializable resourceId;

    private NoResourceException(final Class resourceType, final Serializable resourceId) {
        super(String.format("No Resource '%s' with the identifier '%s' found", resourceType.getSimpleName(), resourceId));
        this.resourceType = resourceType;
        this.resourceId = resourceId;
    }

    public static NoResourceException of(final Class<?> resourceType, final Serializable resourceId) {
        Objects.requireNonNull(resourceType);
        Objects.requireNonNull(resourceId);
        return new NoResourceException(resourceType, resourceId);
    }

    public Class<?> getResourceType() {
        return resourceType;
    }

    public Serializable getResourceId() {
        return resourceId;
    }
}
