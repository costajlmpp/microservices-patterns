package io.costax.core.locale;

import javax.inject.Qualifier;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ PARAMETER, FIELD, METHOD })
@Retention(RUNTIME)
@Documented
@Qualifier
public @interface RequestLocale {
}
