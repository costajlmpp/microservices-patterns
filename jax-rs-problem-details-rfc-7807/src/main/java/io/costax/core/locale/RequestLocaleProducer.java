package io.costax.core.locale;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.io.Serializable;
import java.util.Locale;

@RequestScoped
public class RequestLocaleProducer implements Serializable {

    @Produces
    @RequestLocale
    public Locale requestAcceptLanguage(InjectionPoint ip) {
        Locale locale = LocaleThreadLocal.get();

        if (locale == null) {
            return Locale.getDefault();
        }

        return locale;
    }
}
