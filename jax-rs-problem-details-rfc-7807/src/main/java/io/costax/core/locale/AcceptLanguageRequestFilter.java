package io.costax.core.locale;


import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

/**
 * Checks whether the {@code Accept-Language} HTTP header exists and creates a {@link ThreadLocal} to store the
 * corresponding Locale.
 */
@Provider
@PreMatching
public class AcceptLanguageRequestFilter implements ContainerRequestFilter {

    /*
     * (non-Javadoc)
     * @see javax.ws.rs.container.ContainerRequestFilter#filter(javax.ws.rs.container.ContainerRequestContext)
     */
    @Override
    public void filter(ContainerRequestContext requestContext) {
        if (!requestContext.getAcceptableLanguages().isEmpty()) {
            LocaleThreadLocal.set(requestContext.getAcceptableLanguages().get(0));
        }
    }
}