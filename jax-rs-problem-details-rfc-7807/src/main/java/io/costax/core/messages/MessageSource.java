package io.costax.core.messages;

import java.util.Locale;

public interface MessageSource {

    String getMessage(String code, Locale locale);

    String getMessage(String code, Object[] args, Locale locale);
}
