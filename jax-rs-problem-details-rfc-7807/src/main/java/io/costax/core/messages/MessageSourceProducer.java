package io.costax.core.messages;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class MessageSourceProducer {

    private static final String MESSAGES = "messages";

    @Produces
    @ApplicationScoped
    public MessageSource provideMessageResource() {
        return ResourceBundleMessageSource.of(MESSAGES);
    }
}
