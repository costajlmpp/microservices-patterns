package io.costax.core.messages;

import io.costax.core.locale.RequestLocale;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.Locale;

@RequestScoped
public class RequestLocaleResourceBundleMessageSource {

    @Inject
    MessageSource messageSource;

    @Inject
    @RequestLocale
    Instance<Locale> localeInstance;

    public String getMessage(String code) {
        return messageSource.getMessage(code, localeInstance.get());
    }

    public String getMessage(String code, Object[] args) {
        return messageSource.getMessage(code, args, localeInstance.get());
    }
}
