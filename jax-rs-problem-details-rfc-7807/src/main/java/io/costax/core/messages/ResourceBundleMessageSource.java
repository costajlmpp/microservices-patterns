package io.costax.core.messages;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

public class ResourceBundleMessageSource implements MessageSource {

    private final String basename;
    /**
     * Cache to hold loaded ResourceBundles.
     * This Map is keyed with the bundle basename, which holds a Map that is
     * keyed with the Locale and in turn holds the ResourceBundle instances.
     * This allows for very efficient hash lookups, significantly faster
     * than the ResourceBundle class's own cache.
     */
    private final Map<String, Map<Locale, ResourceBundle>> cachedResourceBundles = new ConcurrentHashMap<>();

    /**
     * Cache to hold already generated MessageFormats.
     * This Map is keyed with the ResourceBundle, which holds a Map that is
     * keyed with the message code, which in turn holds a Map that is keyed
     * with the Locale and holds the MessageFormat values. This allows for
     * very efficient hash lookups without concatenated keys.
     *
     * @see #getMessageFormat
     */
    private final Map<ResourceBundle, Map<String, Map<Locale, MessageFormat>>> cachedBundleMessageFormats = new ConcurrentHashMap<>();

    private ResourceBundleMessageSource(final String basename) {
        this.basename = basename;
    }

    public static ResourceBundleMessageSource of(final String basename) {
        if (basename == null) {
            throw new IllegalArgumentException("The basename should not be null");
        }
        return new ResourceBundleMessageSource(basename);
    }

    @Override
    public String getMessage(final String code, final Locale locale) {
        return getMessage(code, new Object[]{}, locale);
    }

    @Override
    public String getMessage(final String code, final Object[] args, final Locale locale) {
        String msg = getMessageInternal(code, args, locale);

        if (msg == null) {
            return code;
        }

        return msg;
    }

    private String getMessageInternal(String code, Object[] args, Locale locale) {
        if (code == null) {
            return null;
        }

        if (locale == null) {
            locale = Locale.getDefault();
        }

        if (args == null || args.length == 0) {
            // Optimized resolution: no arguments to apply,
            // therefore no MessageFormat needs to be involved.
            return resolveCodeWithoutArguments(code, locale);
        } else {

            MessageFormat messageFormat = resolveCode(code, locale);

            if (messageFormat != null) {
                synchronized (messageFormat) {
                    return messageFormat.format(args);
                }
            }
        }

        return null;
    }

    private MessageFormat resolveCode(final String code, final Locale locale) {
        ResourceBundle bundle = this.getResourceBundle(basename, locale);
        if (bundle != null) {
            return getMessageFormat(bundle, code, locale);
        }
        return null;
    }

    private MessageFormat getMessageFormat(final ResourceBundle bundle, final String code, final Locale locale) {
        Map<String, Map<Locale, MessageFormat>> codeMap = this.cachedBundleMessageFormats.get(bundle);

        Map<Locale, MessageFormat> localeMap = null;
        if (codeMap != null) {
            localeMap = codeMap.get(code);

            if (localeMap != null) {
                MessageFormat result = localeMap.get(locale);

                if (result != null) {
                    return result;
                }
            }
        }

        String msg = getStringOrNull(code, bundle);
        if (msg != null) {
            if (codeMap == null) {
                codeMap = new ConcurrentHashMap<>();
                Map<String, Map<Locale, MessageFormat>> existing =
                        this.cachedBundleMessageFormats.putIfAbsent(bundle, codeMap);
                if (existing != null) {
                    codeMap = existing;
                }
            }

            if (localeMap == null) {
                localeMap = new ConcurrentHashMap<>();
                Map<Locale, MessageFormat> existing = codeMap.putIfAbsent(code, localeMap);
                if (existing != null) {
                    localeMap = existing;
                }
            }
            MessageFormat result = createMessageFormat(msg, locale);
            localeMap.put(locale, result);
            return result;
        }

        return null;
    }

    private MessageFormat createMessageFormat(final String msg, final Locale locale) {
        return new MessageFormat(msg, locale);
    }

    private String resolveCodeWithoutArguments(final String code, final Locale locale) {
        ResourceBundle bundle = this.getResourceBundle(basename, locale);

        if (bundle != null) {
            return getStringOrNull(code, bundle);
        }

        return null;

    }

    private String getStringOrNull(final String code, final ResourceBundle bundle) {
        if (bundle.containsKey(code)) {
            try {
                return bundle.getString(code);
            } catch (MissingResourceException ignored) {
                // Assume key not found for some other reason
                // -> do NOT throw the exception to allow for checking parent message source.
                return null;
            }
        }
        return null;
    }

    private ResourceBundle getResourceBundle(final String basename, final Locale locale) {
        Map<Locale, ResourceBundle> localeMap = this.cachedResourceBundles.get(basename);

        if (localeMap != null) {
            ResourceBundle bundle = localeMap.get(locale);
            if (bundle != null) {
                return bundle;
            }
        }

        ResourceBundle bundle = doGetBundle(basename, locale);
        if (localeMap == null) {
            localeMap = new ConcurrentHashMap<>();
            Map<Locale, ResourceBundle> existing = this.cachedResourceBundles.putIfAbsent(basename, localeMap);
            if (existing != null) {
                localeMap = existing;
            }
        }
        localeMap.put(locale, bundle);
        return bundle;

    }

    private ResourceBundle doGetBundle(final String basename, final Locale locale) {
        return ResourceBundle.getBundle(basename, locale);
    }
}
