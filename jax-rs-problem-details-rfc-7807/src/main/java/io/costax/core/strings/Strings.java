package io.costax.core.strings;

import java.net.URI;
import java.util.UUID;

public class Strings {

    public static String javadocName(Class<?> type) {
        return type.getName()
                .replace('.', '/') // the package names are delimited like a path
                .replace('$', '.'); // nested classes are delimited with a period
    }

    public static String camelToWords(String input) {
        return String.join(" ", input.split("(?=\\p{javaUpperCase})"));
    }

    private URI buildInstance() {
        return URI.create("urn:uuid:" + UUID.randomUUID());
    }
}
