package io.costax.api.exceptions;

import io.costax.api.exceptions.problemdetails.Problem;
import io.costax.api.exceptions.problemdetails.ProblemType;
import io.costax.core.messages.RequestLocaleResourceBundleMessageSource;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.validation.ValidationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.beans.Introspector;
import java.lang.reflect.Proxy;
import java.time.Instant;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {

//    @Inject
//    MessageSource messageSource;
//
//    @Inject
//    @RequestLocale
//    Instance<Locale> requestLocale;

    @Inject
    RequestLocaleResourceBundleMessageSource requestLocaleResourceBundleMessageSource;

    @Override
    public Response toResponse(final ValidationException exception) {
        Response.Status badRequest = Response.Status.BAD_REQUEST;
        ProblemType invalidData = ProblemType.INVALID_DATA;
        return Response.status(badRequest)
                .type(MediaType.APPLICATION_JSON)
                .header("validation-exception", "true")
                .header("X-Reason", invalidData.getTitle())
                .entity(problemOf(badRequest, invalidData, exception))
                .build();
    }

    private Problem problemOf(final Response.Status badRequest, final ProblemType problemType, final ValidationException exception) {
        return Problem
                .createBuilder(badRequest.getStatusCode(), problemType, "One or more properties contains invalid values", Instant.now())
                .errors(createErrors(exception))
                .build();
    }

    private List<Problem.Error> createErrors(ValidationException e) {
        if (e instanceof ConstraintViolationException) {
            return constraintViolationExceptionToErrors((ConstraintViolationException) e);
        }
        return List.of();
    }

    private List<Problem.Error> constraintViolationExceptionToErrors(final ConstraintViolationException e) {
        final Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
        return constraintViolations
                .stream()
                .map(cv -> {


                    final String basePath = Optional
                            .ofNullable(cv.getLeafBean())
                            .map(Object::getClass)
                            .filter(c -> !Proxy.isProxyClass(c))
                            .map(Class::getSimpleName)
                            .filter(s -> !s.contains("$"))
                            .map(Introspector::decapitalize)
                            .orElse("");

                    final String propertyName = Optional
                            .ofNullable(cv.getPropertyPath())
                            .map(Path::toString)
                            .map(s -> {
                                if (s.contains(".")) {
                                    return s.substring(s.lastIndexOf("."));
                                } else {
                                    return s;
                                }
                            })
                            .map(s -> s.replace(".", ""))
                            .orElse("");

                    final String propertyPath = Stream
                            .of(basePath, propertyName)
                            .map(String::trim)
                            .filter(s -> !s.isBlank())
                            .collect(Collectors.joining("."));

                    //basePath + propertyName;

                    final String name = resolveMessageOrTheSame(propertyPath);
                    final String reason = cv.getMessage();

                    return Problem.createError(name, reason, cv.getPropertyPath().toString());
                }).collect(Collectors.toList());
    }

    private String resolveMessageOrTheSame(final String key) {
        try {
            // fixing
            return requestLocaleResourceBundleMessageSource.getMessage(key);
            //return messageSource.getMessage(key, null, LocaleContextHolder.getLocale());
        } catch (MissingResourceException e) {
            return key;
        }

    }


}
