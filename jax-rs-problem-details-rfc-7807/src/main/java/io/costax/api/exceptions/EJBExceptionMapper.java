package io.costax.api.exceptions;

import io.costax.api.exceptions.problemdetails.Problem;
import io.costax.api.exceptions.problemdetails.ProblemType;

import javax.ejb.EJBException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;
import java.time.Instant;
import java.util.Optional;

/**
 * EJB exception mapper handler.<br>
 * This handler will simply unwrap the cause of the exception and delegate
 * to a dedicated handler of the Throwable type if it exists.
 * If there is no dedicated handler then an HTTP 500 will be mapped.
 */
@Provider
public class EJBExceptionMapper implements ExceptionMapper<EJBException> {

    @Context
    private Providers providers;

    @Override
    public Response toResponse(EJBException exception) {
        Exception causedByException = exception.getCausedByException();
        Response response = chain(causedByException);

        if (response == null) {
            return handleExceptionInternal(exception);
        } else {
            return response;
        }
    }

    private Response handleExceptionInternal(final EJBException exception) {
        Response.Status status = Response.Status.INTERNAL_SERVER_ERROR;
        ProblemType problemType = ProblemType.INTERNAL_SERVER_ERROR;

        final String detail = "An unexpected internal system error has occurred. Please try again and if the problem persists contact us.";

        // Importante colocar o printStackTrace (pelo menos por enquanto, que não estamos fazendo logging)
        // para mostrar a stacktrace na console.
        // Se não fizer isso, ficamos sem ver o stacktrace de exceptions que seriam importantes
        // especialmente na fase de desenvolvimento
        exception.printStackTrace();

        Problem problem = Problem.createBuilder(status.getStatusCode(), problemType, detail, Instant.now()).build();

        String reason = Optional
                .ofNullable(exception.getCausedByException())
                .map(Throwable::getMessage)
                .orElse(null);

        return Response
                .serverError()
                .header("X-Reason", reason)
                .type(MediaType.APPLICATION_JSON)
                .entity(problem)
                .build();
    }

    @SuppressWarnings("unchecked")
    private Response chain(Throwable cause) {
        if (cause == null) {
            return null;
        }

        Class aClass = cause.getClass();

        if (WebApplicationException.class.isAssignableFrom(aClass)) {
            return ((WebApplicationException) cause).getResponse();
        }

        ExceptionMapper exceptionMapper = providers.getExceptionMapper(aClass);

        if (exceptionMapper != null) {
            return exceptionMapper.toResponse(cause);
        }

        return chain(cause.getCause());
    }
}