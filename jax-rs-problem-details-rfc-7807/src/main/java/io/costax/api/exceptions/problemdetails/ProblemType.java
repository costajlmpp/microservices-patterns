package io.costax.api.exceptions.problemdetails;

public enum ProblemType {
    INTERNAL_SERVER_ERROR("/internal-server-error", "System Internal Error"),
    INVALID_DATA("/invalid-data", "Invalid data"),
    RESOURCE_NOT_FOUND("/resource-not-found", "Resource not found");

    private final String title;
    private final String uri;

    ProblemType(String path, String title) {
        // rename the base uri...
        this.uri = "https://example.net/" + path;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getUri() {
        return uri;
    }
}
