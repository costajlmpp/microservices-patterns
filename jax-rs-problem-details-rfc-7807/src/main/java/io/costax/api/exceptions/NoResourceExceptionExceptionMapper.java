package io.costax.api.exceptions;

import io.costax.api.exceptions.problemdetails.Problem;
import io.costax.api.exceptions.problemdetails.ProblemType;
import io.costax.core.persistence.NoResourceException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.time.Instant;

@Provider
public class NoResourceExceptionExceptionMapper implements ExceptionMapper<NoResourceException> {

    @Override
    public Response toResponse(final NoResourceException exception) {

        Response.Status notFound = Response.Status.NOT_FOUND;
        ProblemType resourceNotFound = ProblemType.RESOURCE_NOT_FOUND;

        return Response
                .status(notFound)
                .type(MediaType.APPLICATION_JSON)
                .entity(Problem
                        .createBuilder(notFound.getStatusCode(), resourceNotFound, exception.getMessage(), Instant.now())
                        .build())
                .build();
    }
}
