# JAX-RS with MicroProfile Reactive Messaging



1. create the project 
```
Set the project groupId [org.acme.quarkus.sample]: io.github.jlmc
Set the project artifactId [my-quarkus-project]: reactive
Set the project version [1.0-SNAPSHOT]: 
Do you want to create a REST resource? (y/n) [no]: y
Set the resource classname [io.github.jlmc.HelloResource]: 
Set the resource path  [/hello]: 
```


2. In this example we are not using `Rest Assured` tests, so we can remove tests folders, and remove also the maven dependency
```
rm -rf src/test/java/
```


3. Add The necessary quarkus extensions

  - Open-API: [`io.quarkus:quarkus-smallrye-openapi`](https://quarkus.io/guides/openapi-swaggerui)
    ```shell script
    ./mvnw quarkus:add-extension -Dextensions="quarkus-smallrye-openapi"
    ```
    will add the maven dep
    ```xml
    <dependency>
        <groupId>io.quarkus</groupId>
        <artifactId>quarkus-smallrye-openapi</artifactId>
    </dependency>
    ```
    Now:
    ```
    $ curl http://localhost:8080/openapi
    ```
  - smallrye message [`io.quarkus:quarkus-smallrye-reactive-messaging`](https://smallrye.io/smallrye-reactive-messaging)
    ```
    ./mvnw quarkus:add-extension -Dextensions="quarkus-smallrye-reactive-messaging"
    ```
    will add the following maven dep:
    ```
    <dependency>
        <groupId>io.quarkus</groupId>
        <artifactId>quarkus-smallrye-reactive-messaging</artifactId>
    </dependency>
    ```

4. Running 
```shell script
mvn clean compile quarkus:dev
```