package io.github.jlmc;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class HelloListener {

    @Incoming("hello")
    @Outgoing("confirmed")
    public String onHello(String hello) {
        System.out.println("HelloListener#onHello -> " + hello);

        return "confirm:" + hello;
    }
}
