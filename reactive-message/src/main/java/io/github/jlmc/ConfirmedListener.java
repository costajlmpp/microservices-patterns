package io.github.jlmc;

import org.eclipse.microprofile.reactive.messaging.Incoming;

public class ConfirmedListener {

    @Incoming("confirmed")
    public void onConfirmation(String message) {
        System.out.println("ConfirmedListener#onConfirmation -> " + message);
    }
}
