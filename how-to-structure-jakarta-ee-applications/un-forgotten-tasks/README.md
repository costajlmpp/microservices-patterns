# Build
mvn clean package && docker build -t io.costax/un-forgotten-tasks .

# RUN

docker rm -f un-forgotten-tasks || true && docker run -d -p 8080:8080 -p 4848:4848 --name un-forgotten-tasks io.costax/un-forgotten-tasks 

# Microprofile Stuff

* Metrics: http://localhost:8080/metrics, in prometheus format, but we can request another mediaType

```
curl -i -H"Accept: application/json" http://localhost:8080/metrics
```

# documentation
with microprofile we get auto of the box swagger format api documentation:

```
localhost:8080\openapi
```

  