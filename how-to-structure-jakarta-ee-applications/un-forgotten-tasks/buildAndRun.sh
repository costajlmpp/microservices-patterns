#!/bin/sh
mvn clean package && docker build -t io.costax/un-forgotten-tasks .
docker rm -f un-forgotten-tasks || true && docker run -d -p 8080:8080 -p 4848:4848 --name un-forgotten-tasks io.costax/un-forgotten-tasks 
