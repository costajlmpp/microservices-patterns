package io.costax.members;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

@Path("/members")
public class MembersResource {

    @Inject
    Members members;

    @GET
    public List<Member> search() {
        AtomicLong a = new AtomicLong();
        long andIncrement = a.getAndIncrement();

        LongAdder ld = new LongAdder();
        ld.increment();

        return members.search();
    }

}
