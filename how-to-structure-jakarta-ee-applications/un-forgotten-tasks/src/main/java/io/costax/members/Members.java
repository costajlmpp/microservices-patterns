package io.costax.members;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class Members {

    @PersistenceContext
    EntityManager em;

    public List<Member> search() {
        return em.createQuery("select m from Member m", Member.class).getResultList();
    }
}
