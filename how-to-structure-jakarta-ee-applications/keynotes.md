# how-to-structure-jakarta-ee-applications

## Agenda

* reuse
* modularization
* packages
* exceptions
* patterns
* domain specific libraries
* cross cutting functions (metrics, loggins, configurations)

## modularizations 

* think about concepts
* create add value
* void the the tipicly wrong applications names (model, daos, dtos, services, enums ...)

# packages 

* the packages names normaly in plural
* why?
* conventions over configurations
* uselly we use Restfull

* This this a very old pattern called: **Maximal Cohesion Minimal Coupling**
* The packages should gather concepts which are similar to each other and be independent from other packages


# modularization/packages

* Ivar jackcobson in the book "object oriented software engineering" documented a very good pattern, that could be adopted as a standert.
* BCE - Boundary Control Entity
* Robert C Marttin (Uncle Bob) in the book "Clean Arquitecture" rename the pattern to BIE - Boundary Interactor Entity, 
*

## BCE 

![BCE Diagram](doc-resources/bce.png)

* Entities 
 - classes, simple classes, 
 - the getters and setters are simple optionals, in the now days the Jpa and bean validations do not required Getters/Setters any more.
 - We Entity can be purely business objects, we can put business logic, and implements tests cases over.
 - with Java EE 8 we can easy expose the entity via json.


* Boundaries: 
 - Mark the boundaries in the business logic and the rest of the world
 - begin and finish the transactions
 - It is a good practice to have at least to Class in the boundary package, a Facade and a Jax-rs Resource implementation.
 

* Controls 
 - Fully Optionals
 - We should never start with the control
 - It is usual to result from refactory


More About:

* A boundary can communicate with other boundary by an entity can invoke another entity, control can invoke other control and Boundary can control another control
* Entity should not invoke control, controls should not invoke boundaries
* We can have cycles between compoments (normally old arquitectures hate cycles)




# REUSE

 - Specialists aided in its history / experience tell us that reuse only happens in cases of algaritmos such as machine learning or complex calculations
 - Duplication usually happens, and it's not that bad.
 - 

# MODULARIZATION

 - Avoid share jars between microservices
 - why? versions problem ...
 - Entities are not shared between microservices
 - Each mocroservices have it own system-tests, with duplicate the projects entities, this way the clients are completly decoupling.


# EXCEPTIONS
 - With HTTP we can transfere/serialize exceptions over the network
 - exceptions are very util in old deprecated arquitectures systems / like CORBA or RMI, because it are serialized
 - if a exception is throw by a entity then it should be declares in the same pakage of the entity
 - if a exception is throw by a control then should in the same package of the control	

 

https://youtu.be/pMiJSvz2q2c
  

https://youtu.be/pMiJSvz2q2c?t=673



