package io.costax.stocks;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.jms.ConnectionFactory;

@Singleton
public class StockPublisher {

    @Resource(lookup = "jms/__defaultConnectionFactory")
    ConnectionFactory jmsFactory;

    public StockPublisher() {
    }
}
