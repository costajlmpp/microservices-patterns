package io.costax.orders.entity;

public enum OrderState {

    PLACED, STARTED, DELIVERED, CANCELLED
}
