package io.costax.orders.entity;

import io.costax.serialization.FieldAccessStrategy;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.bind.annotation.JsonbVisibility;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;


@JsonbVisibility(FieldAccessStrategy.class)
@Entity
@Table(name = "meal_orders")
public class MealOrder {

    @Id
    //@GeneratedValue
    private String id;

    @Embedded
    @NotNull
    @Valid
    private MealSpecification specification;

    @Enumerated(EnumType.STRING)
    private OrderState state = OrderState.PLACED;

    protected MealOrder() {
    }

    private MealOrder(final Builder builder) {
        this.specification = builder.mealSpecification;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "MealOrder{" +
                "id=" + id +
                ", state=" + state +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof MealOrder)) return false;
        final MealOrder mealOrder = (MealOrder) o;
        return this.id != null && Objects.equals(id, mealOrder.id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public JsonObject toJson() {
        return Json.createObjectBuilder()
                .add("id", this.id)
                .add("status", String.valueOf(this.state))
                .add("specification", this.specification.toJson())
                .build();
    }

    public void start() {
        state = OrderState.STARTED;
    }

    public void cancel() {
        state = OrderState.CANCELLED;
    }

    public void delivery() {
        state = OrderState.DELIVERED;
    }

    public static class Builder {
        private MealSpecification mealSpecification;

        public MealOrder build() {
            return new MealOrder(this);
        }


        public Builder with(final String pizzaId, final int qty, final String extraNotes, final Instant placedAt) {
            this.mealSpecification = MealSpecification.of(pizzaId, qty, extraNotes, placedAt);
            return this;
        }
    }

}
