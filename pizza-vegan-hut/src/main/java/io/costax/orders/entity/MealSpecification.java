package io.costax.orders.entity;

import io.costax.serialization.FieldAccessStrategy;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.bind.annotation.JsonbVisibility;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

@JsonbVisibility(FieldAccessStrategy.class)
@Embeddable
public class MealSpecification implements Serializable {

    @NotNull
    private String pizzaId;

    @Min(1)
    private int qty;

    private String extraNotes;

    private Instant placedAt;

    public MealSpecification() {
    }

    private MealSpecification(final String pizzaId, final int qty, final String extraNotes, final Instant placedAt) {
        this.pizzaId = pizzaId;
        this.qty = qty;
        this.extraNotes = extraNotes;
        this.placedAt = placedAt;
    }

    public static MealSpecification of(final String pizzaId, final int qty, final String extraNotes, final Instant placedAt) {
        return new MealSpecification(pizzaId, qty, extraNotes, placedAt);
    }

    public String getExtraNotes() {
        return extraNotes;
    }

    public void setExtraNotes(final String extraNotes) {
        this.extraNotes = extraNotes;
    }

    public Instant getPlacedAt() {
        return placedAt;
    }

    public void setPlacedAt(final Instant placedAt) {
        this.placedAt = placedAt;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(final int qty) {
        this.qty = qty;
    }

    public String getPizzaId() {
        return pizzaId;
    }

    public void setPizzaId(final String pizzaId) {
        this.pizzaId = pizzaId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof MealSpecification)) return false;
        final MealSpecification that = (MealSpecification) o;
        return qty == that.qty &&
                Objects.equals(pizzaId, that.pizzaId) &&
                Objects.equals(extraNotes, that.extraNotes) &&
                Objects.equals(placedAt, that.placedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(qty, pizzaId, extraNotes, placedAt);
    }

    public JsonObject toJson() {
        return Json.createObjectBuilder()
                .add("pizzaId", this.pizzaId)
                .add("qty", this.qty)
                .add("extraNotes", this.extraNotes)
                .add("placedAt", String.valueOf(this.placedAt))
                .build();


    }
}
