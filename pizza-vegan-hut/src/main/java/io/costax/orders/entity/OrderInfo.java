package io.costax.orders.entity;

public class OrderInfo {

    private String orderId;

    public OrderInfo(final String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }
}
