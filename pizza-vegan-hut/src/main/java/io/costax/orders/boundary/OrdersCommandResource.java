package io.costax.orders.boundary;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

public class OrdersCommandResource {

    @Inject
    OrderService orderService;

    @Context
    UriInfo uriInfo;

    @PUT
    @Path("start")
    public Response start(@PathParam("id") String id) {
        this.orderService.startOrder(id);

        URI uri = getOrderUri(id);

        return Response.accepted().header(HttpHeaders.LOCATION, uri).build();
    }

    @PUT
    @Path("cancel")
    public Response cancel(@PathParam("id") String id, JsonObject reason) {

        final String becauseOf = reason.getString("reason");

        this.orderService.cancelOrder(id, becauseOf);

        URI uri = getOrderUri(id);

        return Response.accepted().header(HttpHeaders.LOCATION, uri).build();
    }

    @PUT
    @Path("delivery")
    public Response delivery(@PathParam("id") String id) {

        this.orderService.deliverMeal(id);

        URI uri = getOrderUri(id);

        return Response.accepted().header(HttpHeaders.LOCATION, uri).build();
    }

    private URI getOrderUri(@PathParam("id") final String id) {
        return uriInfo.getBaseUriBuilder()
                .path(OrdersResource.class)
                .path(OrdersResource.class, "getOrder")
                .build(id);
    }
}
