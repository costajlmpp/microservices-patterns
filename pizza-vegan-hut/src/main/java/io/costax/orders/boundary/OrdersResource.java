package io.costax.orders.boundary;

import io.costax.orders.control.MealOrders;
import io.costax.orders.entity.MealOrder;
import io.costax.orders.entity.OrderState;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.EnumSet;
import java.util.List;

@Path("orders")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrdersResource {

    @Context
    ResourceContext context;

    @Context
    UriInfo uriInfo;

    @Inject
    OrderService orderService;

    @Inject
    MealOrders mealOrders;

    @GET
    public List<MealOrder> getAll() {
        return mealOrders.getOrdersInState(EnumSet.allOf(OrderState.class));
    }

    @GET
    @Path("{id}")
    public MealOrder getOrder(@PathParam("id") String id) {
        final MealOrder order = mealOrders.getById(id);

        if (order == null) {
            throw new NotFoundException();
        }

        return order;
    }

    @POST
    public Response orderMeal(@Valid MealOrder orderDetails) {
        final String id = this.orderService.orderMeal(orderDetails);

        /*
        URI uri = uriInfo.getBaseUriBuilder()
                         .path(OrdersResource.class)
                         .path(OrdersResource.class, "getOrder")
                         .build(orderInfo.getOrderId());
        return Response.created(uri).entity(mealOrder).build();
        */

        final URI uri = uriInfo.getAbsolutePathBuilder().path("{id}").build(String.valueOf(id));
        return Response.accepted().header(HttpHeaders.LOCATION, uri).build();
    }

    @Path("{id}/command")
    public OrdersCommandResource commandResource() {
        return this.context.getResource(OrdersCommandResource.class);
    }
}
