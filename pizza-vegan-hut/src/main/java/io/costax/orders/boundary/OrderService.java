package io.costax.orders.boundary;

import io.costax.orders.control.OrderIdGenerator;
import io.costax.orders.entity.MealOrder;
import io.costax.orders.events.MealDelivered;
import io.costax.orders.events.OrderCancelled;
import io.costax.orders.events.OrderPlaced;
import io.costax.orders.events.OrderStarted;
import io.costax.serialization.FieldAccessStrategy;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.TextMessage;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;

public class OrderService {
    // 1.
    // java:/queue/order-meal

    @Resource(lookup = "java:global/queue/OrderPlacedQ")
    Destination orderPlacedQueue;

    @Resource(lookup = "java:global/queue/OrderStartedQ")
    Destination orderStartedQueue;


    @Resource(lookup = "java:global/queue/MealDeliveredQ")
    Destination mealDeliveredQueue;


    @Resource(lookup = "java:global/queue/OrderCanceledQ")
    Destination orderCanceledQueue;

    @Inject
    JMSContext jmsContext;

    @Inject
    OrderIdGenerator orderIdGenerator;

    String orderMeal(MealOrder mealOrder) {
        mealOrder.setId(orderIdGenerator.generate());

        final JMSProducer producer = jmsContext.createProducer();
        final OrderPlaced event = OrderPlaced.of(mealOrder);

        Jsonb jsonb = JsonbBuilder.create();
        final String json = jsonb.toJson(event);

        final TextMessage message = jmsContext.createTextMessage(json);

        producer.send(orderPlacedQueue, message);

        return mealOrder.getId();
    }

    void startOrder(String orderId) {
        final OrderStarted orderStartedEvent = OrderStarted.of(orderId);

        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withPropertyVisibilityStrategy(new FieldAccessStrategy()));
        final JMSProducer producer = jmsContext.createProducer();
        producer.send(orderStartedQueue, jsonb.toJson(orderStartedEvent));
    }

    void cancelOrder(String orderId, String reason) {
        final OrderCancelled orderCancelled = OrderCancelled.of(orderId, reason);

        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withPropertyVisibilityStrategy(new FieldAccessStrategy()));
        final JMSProducer producer = jmsContext.createProducer();

        producer.send(orderCanceledQueue, jsonb.toJson(orderCancelled));
    }

    void deliverMeal(String orderId) {
        final MealDelivered mealDelivered = MealDelivered.of(orderId);

        Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withPropertyVisibilityStrategy(new FieldAccessStrategy()));
        final JMSProducer producer = jmsContext.createProducer();

        producer.send(mealDeliveredQueue, jsonb.toJson(mealDelivered));
    }
}
