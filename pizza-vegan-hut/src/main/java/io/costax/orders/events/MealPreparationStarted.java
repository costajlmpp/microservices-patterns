package io.costax.orders.events;

import java.time.Instant;

public class MealPreparationStarted extends MealEvent {

    private final String orderId;

    public MealPreparationStarted(String orderId) {
        this.orderId = orderId;
    }

    public MealPreparationStarted(String orderId, Instant instant) {
        super(instant);
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }

}
