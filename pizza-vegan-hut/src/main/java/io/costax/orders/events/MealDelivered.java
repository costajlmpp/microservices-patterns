package io.costax.orders.events;

public class MealDelivered extends MealEvent {

    private String orderId;

    protected MealDelivered() {
    }

    private MealDelivered(final String orderId) {
        this.orderId = orderId;
    }

    public static MealDelivered of(final String orderId) {
        return new MealDelivered(orderId);
    }

    public String getOrderId() {
        return orderId;
    }
}
