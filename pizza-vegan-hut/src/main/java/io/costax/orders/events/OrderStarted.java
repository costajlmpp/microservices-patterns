package io.costax.orders.events;

public class OrderStarted extends MealEvent {

    private String orderId;

    protected OrderStarted() {
    }

    private OrderStarted(String orderId) {
        this.orderId = orderId;
    }

    public static OrderStarted of(final String orderId) {
        return new OrderStarted(orderId);
    }

    public String getOrderId() {
        return orderId;
    }

}
