package io.costax.orders.events;

public class OrderCancelled extends MealEvent {

    private String orderId;
    private String reason;

    protected OrderCancelled() {
    }

    private OrderCancelled(String orderId, String reason) {
        this.orderId = orderId;
        this.reason = reason;
    }

    public static OrderCancelled of(String orderId, String reason) {
        return new OrderCancelled(orderId, reason);
    }

    public String getOrderId() {
        return orderId;
    }

    public String getReason() {
        return reason;
    }

}
