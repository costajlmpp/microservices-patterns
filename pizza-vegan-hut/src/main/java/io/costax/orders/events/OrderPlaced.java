package io.costax.orders.events;

import io.costax.orders.entity.MealOrder;

public class OrderPlaced extends MealEvent {

    private MealOrder orderDetails;

    protected OrderPlaced() {
    }

    public static OrderPlaced of(MealOrder orderDetails) {
        final OrderPlaced orderPlaced = new OrderPlaced();
        orderPlaced.orderDetails = orderDetails;
        return orderPlaced;
    }

    public MealOrder getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(final MealOrder orderDetails) {
        this.orderDetails = orderDetails;
    }
}
