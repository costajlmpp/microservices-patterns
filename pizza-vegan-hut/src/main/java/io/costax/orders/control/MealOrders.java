package io.costax.orders.control;

import io.costax.orders.entity.MealOrder;
import io.costax.orders.entity.OrderState;
import io.costax.orders.events.MealDelivered;
import io.costax.orders.events.OrderCancelled;
import io.costax.orders.events.OrderPlaced;
import io.costax.orders.events.OrderStarted;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Observes;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Stateless
public class MealOrders {

    @PersistenceContext
    EntityManager em;

    /*
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public MealOrder orderMeal(final MealOrder orderInfo) {
        em.persist(orderInfo);
        return orderInfo;
    }
     */

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void onOrderPlaced(@Observes OrderPlaced event) {
        final MealOrder mealOrder = event.getOrderDetails();
        em.persist(mealOrder);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void apply(@Observes OrderStarted event) {
        final String orderId = event.getOrderId();
        final MealOrder mealOrder = em.find(MealOrder.class, orderId);
        mealOrder.start();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void apply(@Observes OrderCancelled event) {
        final String orderId = event.getOrderId();
        final MealOrder mealOrder = em.find(MealOrder.class, orderId);
        mealOrder.cancel();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void apply(@Observes MealDelivered event) {
        final String orderId = event.getOrderId();
        final MealOrder mealOrder = em.find(MealOrder.class, orderId);
        mealOrder.delivery();
    }


    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public MealOrder getById(final String id) {
        return em.find(MealOrder.class, id);
    }

    public List<MealOrder> getOrdersInState(final Set<OrderState> states) {
        if (states.isEmpty()) return Collections.emptyList();

        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<MealOrder> cq = cb.createQuery(MealOrder.class);
        final Root<MealOrder> mealOrder = cq.from(MealOrder.class);

        List<Predicate> statesPredicates = new ArrayList<>();
        for (OrderState state : states) {
            statesPredicates.add(cb.equal(mealOrder.get("state"), state));
        }

        final Predicate or = cb.or(statesPredicates.toArray(new Predicate[0]));

        return em.createQuery(
                cq.where(or)
                        .orderBy(
                                cb.desc(mealOrder.get("state")),
                                cb.desc(mealOrder.get("specification").get("placedAt"))))
                .getResultList();
    }
}
