package io.costax.orders.control;

import javax.enterprise.context.ApplicationScoped;
import java.util.UUID;

@ApplicationScoped
public class OrderIdGenerator {

    public String generate() {
        return String.valueOf(UUID.randomUUID());
    }
}
