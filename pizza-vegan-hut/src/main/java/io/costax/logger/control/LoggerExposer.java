package io.costax.logger.control;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.io.Serializable;
import java.util.logging.Logger;

@ApplicationScoped
public class LoggerExposer implements Serializable {

    @Produces
    @Dependent
    public Logger expose(InjectionPoint ip) {
        final String loggerName = ip.getMember().getDeclaringClass().getName();
        return Logger.getLogger(loggerName);
    }

}
