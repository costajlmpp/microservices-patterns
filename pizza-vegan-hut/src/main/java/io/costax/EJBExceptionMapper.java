package io.costax;

import javax.ejb.EJBException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;

/**
 * EJB exception mapper handler.<br>
 * This handler will simply unwrap the cause of the exception and delegate
 * to a dedicated handler of the Throwable type if it exists.
 * If there is no dedicated handler then an HTTP 500 will be mapped.
 */
@Provider
public class EJBExceptionMapper implements ExceptionMapper<EJBException> {

    @Context
    private Providers providers;

    @Override
    public Response toResponse(EJBException exception) {
        Exception causedByException = exception.getCausedByException();
        return chain(causedByException);
    }

    @SuppressWarnings("unchecked")
    private Response chain(Throwable cause) {
        if (cause == null) {
            return Response.serverError().build();
        }

        Class aClass = cause.getClass();

        if (WebApplicationException.class.isAssignableFrom(aClass)) {
            return ((WebApplicationException) cause).getResponse();
        }

        ExceptionMapper exceptionMapper = providers.getExceptionMapper(aClass);

        if (exceptionMapper != null) {
            return exceptionMapper.toResponse(cause);
        }

        return chain(cause.getCause());
    }
}