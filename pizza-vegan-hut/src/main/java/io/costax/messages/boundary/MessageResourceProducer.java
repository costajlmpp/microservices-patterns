package io.costax.messages.boundary;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.ResourceBundle;

@RequestScoped
public class MessageResourceProducer {

    private static final String MESSAGES = "messages";

    @Inject
    private HttpServletRequest request;

    @Produces
    @RequestScoped
    public MessageResource provideMessageResource() {
        Locale requestLocale = request.getLocale();
        ResourceBundle resourceBundle = ResourceBundle.getBundle(MESSAGES, requestLocale);

        return MessageResource.of(resourceBundle);
    }
}
