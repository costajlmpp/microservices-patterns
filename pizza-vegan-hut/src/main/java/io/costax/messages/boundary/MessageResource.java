package io.costax.messages.boundary;

import javax.enterprise.inject.Vetoed;
import java.text.MessageFormat;
import java.util.ResourceBundle;

@Vetoed
public class MessageResource {

    private ResourceBundle resourceBundle;

    protected MessageResource() {
    }

    private MessageResource(final ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    static MessageResource of(final ResourceBundle resourceBundle) {
        if (resourceBundle == null) {
            throw new IllegalArgumentException("the 'resourceBundle' should not be null");
        }

        return new MessageResource(resourceBundle);
    }

    public String getMessage(String key) {
        return resourceBundle.getString(key);
    }

    public String getMessage(String key, Object... args) {
        final String template = resourceBundle.getString(key);
        return MessageFormat.format(template, args);
    }
}
