package io.costax.serialization;

import javax.json.bind.annotation.JsonbTransient;
import javax.json.bind.config.PropertyVisibilityStrategy;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class FieldAccessStrategy implements PropertyVisibilityStrategy {

    private static boolean isPublicOrProtectedGetterMethod(Method method) {
        if (!method.getName().startsWith("get")) return false;
        if (method.getParameterTypes().length != 0) return false;
        if (void.class.equals(method.getReturnType())) return false;
        if (method.isAnnotationPresent(JsonbTransient.class)) return false;
        return Modifier.isProtected(method.getModifiers()) || Modifier.isPublic(method.getModifiers());
    }

    @Override
    public boolean isVisible(Field field) {
        // Modifier.isProtected(field.getModifiers());
        return !field.getName().startsWith("_persistence_");
    }

    @Override
    public boolean isVisible(Method method) {
        return isPublicOrProtectedGetterMethod(method);
    }

}