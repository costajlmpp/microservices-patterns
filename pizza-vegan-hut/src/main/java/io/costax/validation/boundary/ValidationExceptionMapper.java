package io.costax.validation.boundary;

import io.costax.messages.boundary.MessageResource;
import io.costax.validation.entity.Error;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.validation.ValidationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.beans.Introspector;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {

    @Inject
    MessageResource messageResource;

    @Override
    public Response toResponse(ValidationException exception) {
        return Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON)
                .header("validation-exception", "true")
                .header("reason", getMessages(exception))
                .entity(createErrors(exception))
                .build();
    }

    private List<Error> createErrors(ValidationException exception) {
        return getConstraintViolations(exception).stream()
                .map(this::toError)
                .collect(Collectors.toList());
    }

    private String getMessages(ValidationException exception) {
        return exception.getLocalizedMessage();
        /*return getConstraintViolations(exception).stream()
                .findFirst()
                .map(ConstraintViolation::getMessage)
                .orElse(null);
                */
    }

    private Set<ConstraintViolation<?>> getConstraintViolations(ValidationException exception) {
        if (!ConstraintViolationException.class.isAssignableFrom(exception.getClass())) {
            return Collections.emptySet();
        }

        return ((ConstraintViolationException) exception).getConstraintViolations();
    }

    private io.costax.validation.entity.Error toError(final ConstraintViolation constraintViolation) {
        final String propertyPathPrettyErrorName = getPropertyPathPrettyErrorName(constraintViolation);
        final String userMessage = String.format("%s %s", propertyPathPrettyErrorName, constraintViolation.getMessage());
        final String devMessage = String.format("%s %s", constraintViolation.getPropertyPath(), constraintViolation.getMessage());

        return io.costax.validation.entity.Error.of(userMessage, devMessage);
    }

    private String getPropertyPathPrettyErrorName(final ConstraintViolation constraintViolation) {
        final Path propertyPath = constraintViolation.getPropertyPath();
        final Iterator<Path.Node> iterator = propertyPath.iterator();
        final Iterable<Path.Node> nodes = () -> iterator;

        StringBuilder messageResourceKeyBuilder = new StringBuilder();

        // get leaf bean name
        final Object leafBean = constraintViolation.getLeafBean();
        if (leafBean != null) {
            final Class<?> aClass = leafBean.getClass();

            final String leafBeanName = Introspector.decapitalize(aClass.getSimpleName());
            messageResourceKeyBuilder.append(leafBeanName);
        }

        // get the last node of the violation
        final String lastViolationNodeName = StreamSupport.stream(nodes.spliterator(), false)
                .reduce((first, second) -> second)
                .map(Path.Node::getName)
                .orElse(null);

        if (lastViolationNodeName != null && !lastViolationNodeName.trim().isEmpty()) {
            messageResourceKeyBuilder.append(".").append(lastViolationNodeName);
        }

        final String key = messageResourceKeyBuilder.toString();

        if (key.trim().isEmpty()) {
            return propertyPath.toString();
        }

        try {

            return messageResource.getMessage(key);

        } catch (Exception e) {
            return propertyPath.toString();
        }
    }

}
