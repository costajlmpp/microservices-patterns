package io.costax.validation.entity;

public class Error {

    private final String userMessage;
    private final String devMessage;

    private Error(String userMessage, String devMessage) {
        this.userMessage = userMessage;
        this.devMessage = devMessage;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public String getDevMessage() {
        return devMessage;
    }

    public static Error of(String userMessage, String devMessage) {
        return new Error(userMessage, devMessage);
    }
}
