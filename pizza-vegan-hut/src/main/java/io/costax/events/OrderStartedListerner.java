package io.costax.events;

import io.costax.orders.events.OrderStarted;
import io.costax.serialization.FieldAccessStrategy;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.*;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import java.util.logging.Level;
import java.util.logging.Logger;


@JMSDestinationDefinition(
        name = "java:global/queue/OrderStartedQ",
        interfaceName = "javax.jms.Queue",
        destinationName = "OrderStartedQ"
)
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:global/queue/OrderStartedQ"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class OrderStartedListerner implements MessageListener {

    @Inject
    Logger logger;

    @Inject
    Event<OrderStarted> orderStartedEvent;

    @Override
    public void onMessage(final Message message) {
        try {
            final String json = ((TextMessage) message).getText();

            Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withPropertyVisibilityStrategy(new FieldAccessStrategy()));

            final OrderStarted orderStarted = jsonb.fromJson(json, OrderStarted.class);

            orderStartedEvent.fire(orderStarted);

        } catch (JMSException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }
}
