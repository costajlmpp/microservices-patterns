package io.costax.events;


import io.costax.orders.events.MealDelivered;
import io.costax.serialization.FieldAccessStrategy;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.*;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import java.util.logging.Level;
import java.util.logging.Logger;

@JMSDestinationDefinition(
        name = "java:global/queue/MealDeliveredQ",
        interfaceName = "javax.jms.Queue",
        destinationName = "MealDeliveredQ"
)
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:global/queue/MealDeliveredQ"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class MealDeliveredListerner implements MessageListener {

    // MealDelivered
    @Inject
    Logger logger;

    @Inject
    Event<MealDelivered> mealDeliveredEvent;

    @Override
    public void onMessage(final Message message) {
        try {
            final String json = ((TextMessage) message).getText();

            Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withPropertyVisibilityStrategy(new FieldAccessStrategy()));

            final MealDelivered mealDelivered = jsonb.fromJson(json, MealDelivered.class);

            mealDeliveredEvent.fire(mealDelivered);

        } catch (JMSException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }
}
