package io.costax.events;

import io.costax.orders.events.OrderPlaced;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.*;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.util.logging.Level;
import java.util.logging.Logger;

@JMSDestinationDefinition(
        name = "java:global/queue/OrderPlacedQ",
        interfaceName = "javax.jms.Queue",
        destinationName = "OrderPlacedQ"
)
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:global/queue/OrderPlacedQ"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class OrderPlacedListerner implements MessageListener {

    @Inject
    Event<OrderPlaced> orderPlacedEvent;

    @Inject
    Logger logger;

    @Override
    public void onMessage(final Message message) {
        try {

            TextMessage textMessage = (TextMessage) message;
            Jsonb jsonb = JsonbBuilder.create();

            final OrderPlaced orderPlaced = jsonb.fromJson(textMessage.getText(), OrderPlaced.class);

            orderPlacedEvent.fire(orderPlaced);

        } catch (JMSException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }

    }
}
