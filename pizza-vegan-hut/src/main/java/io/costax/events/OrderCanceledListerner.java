package io.costax.events;

import io.costax.orders.events.OrderCancelled;
import io.costax.serialization.FieldAccessStrategy;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.jms.*;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import java.util.logging.Level;
import java.util.logging.Logger;


@JMSDestinationDefinition(
        name = "java:global/queue/OrderCanceledQ",
        interfaceName = "javax.jms.Queue",
        destinationName = "OrderCanceledQ"
)
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:global/queue/OrderCanceledQ"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class OrderCanceledListerner implements MessageListener {

    @Inject
    Event<OrderCancelled> orderCancelledEvent;

    @Inject
    Logger logger;

    @Override
    public void onMessage(final Message message) {
        try {
            TextMessage textMessage = (TextMessage) message;

            Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withPropertyVisibilityStrategy(new FieldAccessStrategy()));

            final OrderCancelled orderCancelled = jsonb.fromJson(textMessage.getText(), OrderCancelled.class);

            orderCancelledEvent.fire(orderCancelled);

        } catch (JMSException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }
}

