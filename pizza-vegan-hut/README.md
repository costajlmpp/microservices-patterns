# Build
mvn clean package && docker build -t io.costax/pizza-vegan-hut .

# RUN

docker rm -f ee-jms || true && docker run -d -p 8080:8080 -p 4848:4848 --name ee-jms io.costax/ee-jms 


# Run in high productivity

1. Terminal AppServer
```
./asadmin start-domain --verbose domain1

```


2. Terminal wad 

```
export M2_HOME=/usr/local/Cellar/maven/3.6.1/libexec

java -jar ~/Documents/scripts/wad/wad.jar /Users/costax/Documents/servers/payara/payara-5.192/payara5/glassfish/domains/domain1/autodeploy

```

# About

As Payara already comes with [OpenMQ](https://javaee.github.io/openmq/), which implements the Java Message Service (JMS) standard, you don’t have to set up an external JMS broker (e.g ActiveMQ, RabbitMQ ..) for this example and can use the embedded version (think twice if you use this in production).

The connection pool for the embedded OpenMQ is preconfigured and we can directly make use of it via its JNDI name `jms/__defaultConnectionFactory`. 

If you want to connect to an external broker you would have to set up the connection manually (take a look at this [excellent example](https://blog.payara.fish/connecting-to-activemq-with-payara-server) for ActiveMQ from Steve Millidge itself).


With JMS you can make use of two different concepts for delivering our messages: Topics (publish & subscribe) and Queues (point-to-point). In this example, 

I’m using a `javax.jms.Queue` but the code would look quite similar for using a Topic.

The JMS Queue or Topic has to be first configured within **Payara** as a J**MS Destination Resource** and can be configured either via the Payara admin panel (Resources – JMS Resources – Destination Resources) or using `asadmin`:

```
asadmin create-jms-resource --restype javax.jms.Queue --property Name=STOCKS jms/stocks
```


You have to specify the resource type (Topic or Queue), the physical name of the resource within the broker (will be created if it doesn’t exist) and the JNDI name.



