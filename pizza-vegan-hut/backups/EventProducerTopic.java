package io.costax.events;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.json.Json;
import javax.json.JsonObject;

public class EventProducerTopic {

    @Resource
    ManagedExecutorService mes;

    @Inject
    JMSContext jmsContext;

    @Resource(lookup = "java:global/queue/simpleQ")
    Destination checkoutsTopic;

    //@Inject
    //Topic topic;


    public void send() {

        System.out.println("---");
        JMSProducer producer = jmsContext.createProducer();

        final JsonObject json = Json.createObjectBuilder().add("id", "1").add("nick", "duke").build();

        producer.send(this.checkoutsTopic, json.toString());

        //producer.send(checkoutsTopic, "Hele helo " + LocalDateTime.now());

    }

}
