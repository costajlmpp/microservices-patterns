package io.costax.events;


import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.*;

@JMSDestinationDefinition(
        name = "java:global/queue/simpleQ",
        interfaceName = "javax.jms.Queue",
        destinationName = "simpleQ"
)
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:global/queue/simpleQ"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})

public class TopicListener implements MessageListener {

    @Override
    public void onMessage(final Message message) {
        TextMessage textMessage = (TextMessage) message;

        try {

            System.out.println("start reciving");

            try {
                Thread.sleep(30000L);
            } catch (InterruptedException ignored) {
                ignored.printStackTrace();
            }

            System.out.println("A new stock information arrived: " + textMessage.getText());
//
//            JsonReader jsonReader = Json.createReader(new StringReader(textMessage.getText()));
//            JsonObject stockInformation = jsonReader.readObject();
//
//            em.persist(new StockHistory(stockInformation));
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}



