package io.costax.events;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.jms.*;
import javax.json.Json;
import javax.json.JsonObject;
import java.time.Instant;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Logger;

@ApplicationScoped
public class EventProducer {

    @Inject
    Logger logger;
    @Resource(lookup = "jms/__defaultConnectionFactory")
    private ConnectionFactory jmsFactory;
    @Resource(lookup = "jms/stocks")
    private Queue jmsQueue;

    //@Inject
    //JMSContext jmsContext;

    void onAppStartUp(@Observes @Initialized(ApplicationScoped.class) final Object doesNotMatter) {
        logger.info("Starting up");
    }


    public void send() {
        TextMessage message;

        try (Connection connection = jmsFactory.createConnection();
             Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
             //Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);


             MessageProducer producer = session.createProducer(jmsQueue)) {


            String[] stockCodes = {"MSFT", "GOOGL", "AAPL", "AMZN"};


            JsonObject stockInformation = Json.createObjectBuilder()
                    .add("stockCode", stockCodes[ThreadLocalRandom.current().nextInt(stockCodes.length)])
                    .add("price", ThreadLocalRandom.current().nextDouble(1.0, 150.0))
                    .add("timestamp", Instant.now().toEpochMilli()).build();


            message = session.createTextMessage();
            message.setText(stockInformation.toString());
            producer.send(message);

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }


}
