package io.costax.events;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.jms.JMSDestinationDefinition;


@JMSDestinationDefinitions(
        value = {
                @JMSDestinationDefinition(
                        name = "java:/queue/Queue1",
                        interfaceName = "javax.jms.Queue",
                        destinationName = "duke-queue"
                ),
                @JMSDestinationDefinition(
                        name = "java:/queue/Topic1",
                        interfaceName = "javax.jms.Topic",
                        destinationName = "duke-topic"
                )
        }
)
@Startup
@Singleton
public class JMSConfiguration {

    @Resource(lookup = "java:/queue/Queue1")
    private Queue queue;

    @Resource(lookup = "java:/queue/Topic1")
    private Topic topic;


    @Produces
    public Queue exposeQueue() {
        return this.queue;
    }

    @Produces
    public Topic exposeTopic() {
        return this.topic;
    }
}


