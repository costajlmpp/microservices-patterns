# Build
mvn clean package && docker build -t io.costax/kafka-integration .

# RUN

docker rm -f kafka-integration || true && docker run -d -p 8080:8080 -p 4848:4848 --name kafka-integration io.costax/kafka-integration 


# start all enviroment 


[documentation](https://github.com/wurstmeister/kafka-docker/wiki/Connectivity)

Start a Kafka cluster:
```
docker-compose up -d
```

To add more Kafka brokers:
```
docker-compose scale kafka=3
```

To destroy a cluster:
```
docker-compose stop
```

# http://cloudurable.com/blog/kafka-tutorial-kafka-producer/index.html
# https://dzone.com/articles/writing-a-kafka-consumer-in-java

## To create a new topic named my-example-topic
bash-4.4# kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 4 --topic my-example-topic
 
## To start a producer that publishes datastream from standard input to kafka
bash-4.4# kafka-console-producer.sh --broker-list localhost:9092 --topic my-example-topic


kafka-topics.sh --create --zookeeper localhost:2181 \
  --replication-factor 1 --partitions 13 \
  --topic my-topic


kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 3 --partitions 3 --topic my-example-topic3
