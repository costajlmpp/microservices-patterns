#!/bin/sh
mvn clean package && docker build -t io.costax/kafka-integration .
docker rm -f kafka-integration || true && docker run -d -p 8080:8080 -p 4848:4848 --name kafka-integration io.costax/kafka-integration 
