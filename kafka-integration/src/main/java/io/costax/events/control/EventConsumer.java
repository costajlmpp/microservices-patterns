package io.costax.events.control;

import io.costax.events.entity.MealEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;

import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class EventConsumer implements Runnable {

    private final KafkaConsumer<String, MealEvent> consumer;
    private final Consumer<MealEvent> eventConsumer;
    private final AtomicBoolean closed = new AtomicBoolean();


    public EventConsumer(Properties kafkaProperties, Consumer<MealEvent> eventConsumer, String... topics) {
        this.eventConsumer = eventConsumer;
        consumer = new KafkaConsumer<>(kafkaProperties);
        consumer.subscribe(Arrays.asList(topics));
    }

    @Override
    public void run() {
        try {
            
            while (!closed.get()) {
                
                consume();
            }
            
        } catch (WakeupException e) {
            // will wakeup for closing
        } finally {
            consumer.close();
        }
    }

    private void consume() {
        ConsumerRecords<String, MealEvent> records = consumer.poll(Long.MAX_VALUE);

        for (ConsumerRecord<String, MealEvent> record : records) {
            this.eventConsumer.accept(record.value());
        }

        consumer.commitAsync();
    }

    public void stop() {
        closed.set(true);
        consumer.wakeup();
    }
}
