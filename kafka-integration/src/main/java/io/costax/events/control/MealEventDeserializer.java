package io.costax.events.control;

import io.costax.events.entity.MealEvent;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.Map;

public class MealEventDeserializer implements Deserializer<MealEvent> {

    @Override
    public void configure(final Map<String, ?> configs, final boolean isKey) {

    }

    @Override
    public MealEvent deserialize(final String topic, final byte[] data) {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(data);
             ObjectInput in = new ObjectInputStream(bis)) {
            final MealEvent o = (MealEvent) in.readObject();
            return o;
        } catch (ClassNotFoundException | IOException e) {
            throw new SerializationException("Error when serializing Customer to byte[] " + e);
        }
    }

    @Override
    public void close() {

    }
}
