package io.costax.events.control;

import io.costax.events.entity.MealEvent;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.ProducerFencedException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Properties;
import java.util.concurrent.Future;

@ApplicationScoped
public class EventProducer {

    @Inject
    Properties kafkaProperties;

    private Producer<String, MealEvent> producer;
    private String topic = "topics.order";

    @PostConstruct
    public void initialize() {
        producer = new KafkaProducer<>(kafkaProperties);
        //topic = kafkaProperties.getProperty("topics.order");
        //producer.initTransactions();
    }

    @PreDestroy
    public void close() {
        this.producer.close();
    }

    public void publish(final MealEvent event) {
        ProducerRecord<String, MealEvent> record = new ProducerRecord<>(topic, event);



        try {


            //producer.beginTransaction();
            producer.send(record);
            //producer.commitTransaction();

        } catch (ProducerFencedException e) {
            producer.close();
        } catch (KafkaException e) {
            producer.abortTransaction();
        }
    }
}
