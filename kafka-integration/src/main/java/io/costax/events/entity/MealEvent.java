package io.costax.events.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public abstract class MealEvent implements Serializable {

    private final Instant instant;

    protected MealEvent() {
        instant = Instant.now();
    }

    protected MealEvent(Instant instant) {
        Objects.requireNonNull(instant);
        this.instant = instant;
    }

    public Instant getInstant() {
        return instant;
    }
}
