package io.costax.orders.entity;

import javax.persistence.*;

@Entity
@Table(name = "meal_orders")
public class MealOrder {

    @Id
    private String orderId;

    @Enumerated(EnumType.STRING)
    private OrderState state;

    @Embedded
    private MealSpecification specification;

    public String getOrderId() {
        return orderId;
    }

    public OrderState getState() {
        return state;
    }
}
