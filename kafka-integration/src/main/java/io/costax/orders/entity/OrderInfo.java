package io.costax.orders.entity;

import java.io.Serializable;
import java.util.UUID;

public class OrderInfo implements Serializable {

    private final UUID orderId;

    public OrderInfo(UUID orderId) {
        this.orderId = orderId;
    }

    public UUID getOrderId() {
        return orderId;
    }
}
