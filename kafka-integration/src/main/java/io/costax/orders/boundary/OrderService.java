package io.costax.orders.boundary;

import io.costax.events.control.EventProducer;
import io.costax.events.entity.MealDelivered;
import io.costax.events.entity.OrderCancelled;
import io.costax.events.entity.OrderPlaced;
import io.costax.events.entity.OrderStarted;
import io.costax.orders.entity.OrderInfo;

import javax.inject.Inject;
import java.util.UUID;

public class OrderService {

    @Inject
    EventProducer eventProducer;

    public void orderMeal(final OrderInfo orderInfo) {
        eventProducer.publish(new OrderPlaced(orderInfo));
    }

    void cancelOrder(UUID orderId, String reason) {
        eventProducer.publish(new OrderCancelled(orderId, reason));
    }

    void startOrder(UUID orderId) {
        eventProducer.publish(new OrderStarted(orderId));
    }

    void deliverMeal(UUID orderId) {
        eventProducer.publish(new MealDelivered(orderId));
    }
}
