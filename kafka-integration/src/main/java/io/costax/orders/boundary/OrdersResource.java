package io.costax.orders.boundary;

import io.costax.orders.control.MealOrders;
import io.costax.orders.entity.MealOrder;
import io.costax.orders.entity.OrderInfo;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.UUID;

@Path("orders")
@RequestScoped
public class OrdersResource {

    @Inject
    MealOrders mealOrders;

    @Inject
    OrderService orderService;

    @Context
    UriInfo uriInfo;

    /**
     * Orders Query Resource
     */
    @GET
    @Path("{id}")
    public JsonObject getOrder(@PathParam("id") UUID id) {
        MealOrder mealOrder = mealOrders.get(id);

        if (mealOrder == null) {
            throw new NotFoundException();
        }

        return Json.createObjectBuilder()
                .add("id", mealOrder.getOrderId())
                .add("status", mealOrder.getState().name())
                .build();
    }

    /**
     * Orders Command Resource
     */
    @POST
    public Response orderMeal(JsonObject order) {
        OrderInfo orderInfo = createOrderInfo(order);
        orderService.orderMeal(orderInfo);

        URI uri = uriInfo.getBaseUriBuilder()
                .path(OrdersResource.class)
                .path(OrdersResource.class, "getOrder")
                .build(orderInfo.getOrderId());

        return Response.accepted().header(HttpHeaders.LOCATION, uri).build();
    }

    private OrderInfo createOrderInfo(JsonObject order) {
        // ...
        return new OrderInfo(UUID.randomUUID());
    }
}
