package io.costax.orders.boundary;

import io.costax.events.control.EventConsumer;
import io.costax.events.entity.MealEvent;
import io.costax.events.entity.OrderPlaced;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.Properties;
import java.util.UUID;

//@Singleton
//@Startup
public class OrderEventHandler {

    private EventConsumer eventConsumer;

    @Resource
    ManagedExecutorService mes;

    @Inject
    Properties kafkaProperties;

    @Inject
    OrderService orderService;

    @Inject
    Event<MealEvent> events;


//    @PostConstruct
//    private void init() {
//        kafkaProperties.put("group.id", "order-handler");
//        String chef = kafkaProperties.getProperty("chef.topic");
//
//        eventConsumer = new EventConsumer(kafkaProperties, ev -> events.fire(ev), chef);
//
//        mes.execute(eventConsumer);
//    }
//
//    @PreDestroy
//    public void close() {
//        eventConsumer.stop();
//    }


    public void handle(@Observes OrderPlaced event) {

        final UUID orderId = event.getOrderInfo().getOrderId();

        orderService.startOrder(orderId);

        System.out.println("---");
    }
}
