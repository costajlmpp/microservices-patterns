package io.costax.orders.control;

import io.costax.orders.entity.MealOrder;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.UUID;

@Stateless
public class MealOrders {

    @PersistenceContext
    EntityManager em;

    public MealOrder get(final UUID id) {
        return em.find(MealOrder.class, String.valueOf(id));
    }

    public void apply() {

    }
}
