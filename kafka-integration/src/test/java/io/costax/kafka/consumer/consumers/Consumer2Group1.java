package io.costax.kafka.consumer.consumers;

import io.costax.kafka.consumer.Constants;

public class Consumer2Group1 extends AbstractKafkaConsumerExample {

    protected Consumer2Group1(final String... topics) {
        super("Consumer-Group-1", "Consumer-2-g1", topics);
    }

    public static void main(String[] args) {
        new Consumer2Group1(Constants.TOPIC).startConsume();
    }
}
