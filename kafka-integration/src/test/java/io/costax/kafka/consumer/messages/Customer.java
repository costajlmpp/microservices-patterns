package io.costax.kafka.consumer.messages;

import java.io.Serializable;

public class Customer implements Serializable {

    private int customerID;
    private String customerName;

    public Customer(final int customerID, final String customerName) {
        this.customerID = customerID;
        this.customerName = customerName;
    }

    public int getID() {
        return customerID;
    }
    public String getName() {
        return customerName;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerID=" + customerID +
                ", customerName='" + customerName + '\'' +
                '}';
    }
}
