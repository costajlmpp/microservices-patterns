package io.costax.kafka.consumer.avroserialization;

import io.costax.kafka.consumer.messages.Customer;

import java.util.Random;

public class CustomerGenerator {


    static int min = 111;
    static int max = 999;

    public static Customer getNext() {


        int id = generateId(min, max);

        return new Customer(id, "ABC - " + id);
    }

    private static int generateId(final int min, final int max) {
        Random r = new Random();
        return r.ints(min, (max + 1)).findFirst().getAsInt();
    }
}
