package io.costax.kafka.consumer.consumers;

import io.costax.kafka.consumer.Constants;

public class Consumer1Group1 extends AbstractKafkaConsumerExample {


    protected Consumer1Group1(final String... topics) {
        super("Consumer-Group-1", "Consumer-1-g1", topics);
    }

    public static void main(String[] args) {
        new Consumer1Group1(Constants.TOPIC).startConsume();
    }
}
