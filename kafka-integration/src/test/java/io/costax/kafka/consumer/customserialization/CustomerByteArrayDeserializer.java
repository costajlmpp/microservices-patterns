package io.costax.kafka.consumer.customserialization;

import io.costax.kafka.consumer.messages.Customer;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import java.nio.ByteBuffer;
import java.util.Map;

public class CustomerByteArrayDeserializer implements Deserializer<Customer> {

    @Override
    public void configure(final Map<String, ?> configs, final boolean isKey) {
        // nothing to do
    }

    @Override
    public Customer deserialize(final String topic, final byte[] data) {
        try {

            if (data == null) {
                return null;
            }

            if (data.length < 8) {
                throw new SerializationException("Size of data received by IntegerDeserializer is shorter than expected");
            }

            ByteBuffer buffer = ByteBuffer.wrap(data);

            int id = buffer.getInt();
            int nameSize = buffer.getInt();
            byte[] nameBytes = new byte[nameSize];
            buffer.get(nameBytes);
            String name = new String(nameBytes);

            return new Customer(id, name);

        } catch (Exception e) {
            throw new SerializationException("Error when serializing Customer to byte[] " + e);
        }
    }

    @Override
    public void close() {
        // nothing
    }
}
