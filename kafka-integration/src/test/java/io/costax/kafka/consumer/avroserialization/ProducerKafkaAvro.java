package io.costax.kafka.consumer.avroserialization;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import io.costax.kafka.consumer.messages.Customer;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

import static io.costax.kafka.consumer.Constants.BOOTSTRAP_SERVERS;

public class ProducerKafkaAvro {

    public static final String schemaUrl = "localhost:8081";
    public static final String topic = "customerContacts";
    public static final int wait = 500;


    public static void main(String[] args) {
        // We keep producing new events until someone ctrl-c

        //main1(args);
       main2(args);
    }

    public static void main2(String[] args) {





        // We keep producing new events until someone ctrl-c


        Properties props = new Properties();

        // "bootstrap.servers"
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        //props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        // "key.serializer"
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        // "value.serializer"
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());


        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaAvroExampleProducer");


        props.put("schema.registry.url", schemaUrl);

        ////////////////

        final Schema customerSchema = getCustomerSchema();
        Producer<String, GenericRecord> producer = new KafkaProducer<String, GenericRecord>(props);

        //final Producer<String, Customer> producer = createProducerGeneric();

        while (true) {

            Customer customer = CustomerGenerator.getNext();

            System.out.println("Generated customer " + customer.toString());




            final ProducerRecord<String, Customer> record = new ProducerRecord<String, Customer>(topic, ""+ customer.getID(), customer);

           // producer.send(record);
        }
    }


    public static void main1(String[] args) {
        // We keep producing new events until someone ctrl-c

        final Producer<String, Customer> producer = createProducer();

        while (true) {

            Customer customer = CustomerGenerator.getNext();

            System.out.println("Generated customer " + customer.toString());

            final ProducerRecord<String, Customer> record = new ProducerRecord<String, Customer>(topic, ""+ customer.getID(), customer);

            producer.send(record);
        }
    }

    public static Schema getCustomerSchema() {
        String schemaString = "{\"namespace\": \"customerManagement.avro\", \"type\": \"record\", " +
                "\"name\": \"Customer\"," +
                "\"fields\": [" +
                "{\"name\": \"ID\", \"type\": \"int\"}," +
                "{\"name\": \"name\", \"type\": \"string\"}," +
                "{\"name\": \"email\", \"type\": [\"null\",\"string\"], \"default\":\"null\" }" +
                "]}";

        final Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(schemaString);

        return schema;
    }


    private static <K,V> org.apache.kafka.clients.producer.Producer<K, V> createProducer() {
        Properties props = new Properties();

        // "bootstrap.servers"
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        // "key.serializer"
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        // "value.serializer"
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());


        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaAvroExampleProducer");


        props.put("schema.registry.url", schemaUrl);




        return new KafkaProducer<>(props);
    }
}
