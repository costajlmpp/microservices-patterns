package io.costax.kafka.consumer.customserialization;

import io.costax.kafka.consumer.Constants;
import io.costax.kafka.consumer.messages.Customer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Properties;

import static io.costax.kafka.consumer.Constants.BOOTSTRAP_SERVERS;

public class ConsumerWithCustomerDeserializer {

    private static final String CONSUMER_GROUP_1 = "CONSUMER_GROUP_B1";
    private Consumer<Long, Customer> consumer;

    public static void main(String[] args) {
        new ConsumerWithCustomerDeserializer().startConsume();
    }

    public void startConsume() {

        this.consumer = createConsumer();

        final int giveUp = 100;   int noRecordsCount = 0;
        while (true) {


            //final ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000);
            final ConsumerRecords<Long, Customer> consumerRecords = consumer.poll(Duration.of(1000, ChronoUnit.MILLIS));

            if (consumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            consumerRecords.forEach(record -> {

                final Customer value = record.value();

                System.out.printf("Consumer [%s] ==> Record:(%d, %s, %d, %d)\n",
                        value.getID() + "---" + value.getName(),
                        record.key(),
                        record.value(),
                        record.partition(),
                        record.offset());
            });

            consumer.commitAsync();
        }

        consumer.close();
        System.out.println("DONE");
    }



    private Consumer<Long, Customer> createConsumer() {
        final Properties props = new Properties();


        /*
         * Notice that KafkaConsumerExample imports LongDeserializer which gets configured as the Kafka record key deserializer,
         * and imports StringDeserializer which gets set up as the record value deserializer.
         *
         * The constant BOOTSTRAP_SERVERS gets set to localhost:9092,localhost:9093,localhost:9094
         * which is the three Kafka servers that we started up. Be ahead and make sure all three Kafka servers are running.
         *
         * The constant TOPIC gets set to the replicated Kafka topic that you created in the last tutorial.
         */
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);

        props.put(ConsumerConfig.GROUP_ID_CONFIG, CONSUMER_GROUP_1);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CustomerByteArrayDeserializer.class.getName());

        // Create the consumer using props.
        final Consumer<Long, Customer> consumer = new KafkaConsumer<>(props);
        // Subscribe to the topic.

        consumer.subscribe(Collections.singletonList(Constants.TOPIC_BYTES_1));

        return consumer;
    }
}
