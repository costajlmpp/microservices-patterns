package io.costax.kafka.consumer;

public final class Constants {

    public final static String TOPIC = "my-example-topic3";

    public final static String TOPIC_BYTES_1 = "my-example-topic-bytes-2";

    public final static String BOOTSTRAP_SERVERS ="localhost:9092,localhost:9093,localhost:9094";
}
