package io.costax.kafka.consumer.olds;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Collections;
import java.util.Properties;

import static io.costax.kafka.consumer.Constants.BOOTSTRAP_SERVERS;
import static io.costax.kafka.consumer.Constants.TOPIC;

public class ConsumerExample2 {

    public static void main(String[] args) throws InterruptedException {

        runConsumer();

    }

    static void runConsumer() throws InterruptedException {
        final Consumer<Long, String> consumer = createConsumer();
        final int giveUp = 100;   int noRecordsCount = 0;


        while (true) {


            //final ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000);
            final TemporalUnit s = ChronoUnit.MILLIS;
            final ConsumerRecords<Long, String> consumerRecords = consumer.poll(Duration.of(1000, s));

            if (consumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            consumerRecords.forEach(record -> {
                System.out.printf("Consumer 2 Record:(%d, %s, %d, %d)\n",
                        record.key(), record.value(),
                        record.partition(), record.offset());
            });

            consumer.commitAsync();
        }
        consumer.close();
        System.out.println("DONE");
    }

    private static Consumer<Long, String> createConsumer() {
        final Properties props = new Properties();


        /*
         * Notice that KafkaConsumerExample imports LongDeserializer which gets configured as the Kafka record key deserializer,
         * and imports StringDeserializer which gets set up as the record value deserializer.
         *
         * The constant BOOTSTRAP_SERVERS gets set to localhost:9092,localhost:9093,localhost:9094
         * which is the three Kafka servers that we started up. Be ahead and make sure all three Kafka servers are running.
         *
         * The constant TOPIC gets set to the replicated Kafka topic that you created in the last tutorial.
         */
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaExampleConsumer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        // Create the consumer using props.
        final Consumer<Long, String> consumer = new KafkaConsumer<>(props);
        // Subscribe to the topic.

        consumer.subscribe(Collections.singletonList(TOPIC));
        return consumer;
    }
}
