package io.costax.kafka.consumer.producers;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.concurrent.Future;
import java.util.function.BiFunction;

import static io.costax.kafka.consumer.Constants.BOOTSTRAP_SERVERS;
import static io.costax.kafka.consumer.Constants.TOPIC;


/**
 * <h1></h1>Sending a Message Asynchronously</h1>
 *
 * Suppose the network roundtrip time between our application and the Kafka cluster is10ms.
 *
 * If we wait for a reply after sending each message, sending 100 messages will take around 1 second.
 *
 * On the other hand, if we just send all our messages and not wait for any replies, then sending 100 messages will
 * barely take any time at all.
 * In most cases, we really don’t need a reply—Kafka sends back the topic, partition, and
 * offset of the record after it was written, which is usually not required by the sending app.
 *
 * On the other hand, we do need to know when we failed to send a message completely so we can throw an exception,
 * log an error, or perhaps write the message to an “errors” file for later analysis.
 *
 * In order to send messages asynchronously and still handle error scenarios, the producer supports adding a
 * callback when sending a record. Here is an example of how  we use a callback:
 */
public class ProducerAsynWithCallback {

    public static void main(String[] args) throws Exception {

        Callback errorHandlerCallback = new Callback() {
            @Override
            public void onCompletion(final RecordMetadata metadata, final Exception exception) {
                if (exception != null) {
                    exception.printStackTrace();
                }
            }
        };

        runProducer(10, errorHandlerCallback);
    }

    private static Producer<Long, String> createProducer() {
        Properties props = new Properties();

        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        return new KafkaProducer<>(props);
    }

    static void runProducer(final int sendMessageCount, final Callback errorHandlerCallback) throws Exception {
        final Producer<Long, String> producer = createProducer();
        long time = System.currentTimeMillis();

        try {
            for (long index = time; index < time + sendMessageCount; index++) {

                final ProducerRecord<Long, String> record = new ProducerRecord<>(TOPIC, index,"Hello Mom " + index);


                /*
                 * KafkaProducer has two types of errors.
                 *
                 * Retriable errors:
                 *      are those that can be resolved
                 *      by sending the message again. For example, a connection error can be resolved
                 *      because the connection may get reestablished.
                 *
                 * A “no leader” error:
                 *      can be resolved when a new leader is elected for the partition.
                 *
                 *
                 *
                 * KafkaProducer can be configured t retry those errors automatically,
                 *      so the application code will get retriable exceptions
                 *      only when the number of retries was exhausted and the error was not resolved.
                 *
                 * Some errors will not be resolved by retrying. For example, “message size too large.” In those
                 * cases, KafkaProducer will not attempt a retry and will return the exception immedi‐ately.
                 */


                final Future<RecordMetadata> metadataFuture = producer.send(record, errorHandlerCallback);
                RecordMetadata metadata = metadataFuture.get();

                long elapsedTime = System.currentTimeMillis() - time;

                System.out.printf("sent record(key=%s value=%s) " +
                                "meta(partition=%d, offset=%d) time=%d\n",
                        record.key(),
                        record.value(),
                        metadata.partition(),
                        metadata.offset(),
                        elapsedTime);

            }
        } finally {
            producer.flush();
            producer.close();
        }
    }
}