package io.costax.kafka.consumer.consumers;

import io.costax.kafka.consumer.Constants;

public class Consumer1Group2 extends AbstractKafkaConsumerExample {

    protected Consumer1Group2(final String... topics) {
        super("Consumer-Group-2", "Consumer-1-g2", topics);
    }

    public static void main(String[] args) {
        new Consumer1Group2(Constants.TOPIC).startConsume();
    }
}
